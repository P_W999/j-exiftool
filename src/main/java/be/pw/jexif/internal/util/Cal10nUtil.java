/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.util;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.google.common.base.Preconditions;

import be.pw.jexif.enums.Errors;
import ch.qos.cal10n.IMessageConveyor;
import ch.qos.cal10n.MessageConveyor;
import ch.qos.cal10n.MessageConveyorException;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

/**
 * A utility for i18n'ing the API's output. <br />
 * Uses Cal10n: <a href="http://cal10n.qos.ch/">http://cal10n.qos.ch/</a>
 *
 * @author phillip
 */
@Slf4j
@UtilityClass
public final class Cal10nUtil {

	/**
	 * The application wide MessageConveyor.
	 */
	private static IMessageConveyor mc;
	/**
	 * List containing the supported Languages.
	 */
	private static final List<Locale> SUPPORTEDLANGUAGES;

	static {
		SUPPORTEDLANGUAGES = new ArrayList<>(4);
		SUPPORTEDLANGUAGES.add(Locale.ENGLISH);
		SUPPORTEDLANGUAGES.add(Locale.US);
		SUPPORTEDLANGUAGES.add(Locale.UK);
		SUPPORTEDLANGUAGES.add(new Locale("nl"));
		Locale l = Locale.getDefault();
		if (!SUPPORTEDLANGUAGES.contains(l)) {
			log.info(MessageFormat.format("Default locale {0} was not supported, falling back to english", l));
			l = Locale.ENGLISH;
		}
		l = new Locale(l.getLanguage()); // UK, US, ... whatever :P
		mc = new MessageConveyor(l);
	}

	public static void changeLocale(final Locale l) {
		Preconditions.checkNotNull(l, "Provided locale can not be null");
		Locale loc = l;
		if (!SUPPORTEDLANGUAGES.contains(loc)) {
			log.info(MessageFormat.format("The locale {0} was not supported, falling back to english", loc));
			loc = Locale.ENGLISH;
		}
		mc = new MessageConveyor(loc);
	}

	/**
	 * Returns an i18n'ed error message.
	 *
	 * @param error the error-message.
	 * @param params the different parameters for the error-message.
	 * @return the i18n'ed error message with the parameters filled in.
	 * @see MessageFormat
	 */
	public static String get(final Errors error, final Object... params) {
		try {
			return mc.getMessage(error, params);
		} catch (MessageConveyorException e) {
			StringBuilder b = new StringBuilder();
			b.append(error.toString()).append(":").append(ArrayUtil.toString(params));
			return b.toString();
		}
	}

	/**
	 * Returns a list of supported languages.
	 *
	 * @return the list of supported locales
	 */
	public static List<Locale> getSupportedLanguages() {
		return SUPPORTEDLANGUAGES;
	}
}
