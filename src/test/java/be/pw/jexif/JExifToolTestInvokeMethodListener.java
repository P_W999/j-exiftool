package be.pw.jexif;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

/**
 * Invoke listener which prints out testnames.
 * <p>
 * This listener was added so one could see that the JExifTool tests are running (and not blocking) in maven.
 * 
 * @author phillip
 *
 */
public class JExifToolTestInvokeMethodListener implements IInvokedMethodListener {

	/**
	 * Logger.
	 */
	private static final Logger LOG = LoggerFactory.getLogger("TestNg");

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void afterInvocation(IInvokedMethod arg0, ITestResult arg1) {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void beforeInvocation(IInvokedMethod arg0, ITestResult arg1) {
		if (arg0.isTestMethod()) {
			try {
				LOG.info("Starting test " + Class.forName(arg0.getTestMethod().getTestClass().getName()).getSimpleName() + ":" + arg0.getTestMethod().getMethodName());
			} catch (ClassNotFoundException e) {
				LOG.error("Failed to print testname", e);
			}
		}
	}

}
