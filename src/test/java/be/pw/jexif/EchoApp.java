/*******************************************************************************
 * Copyright 2014 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Duration;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.Executor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.exec.ShutdownHookProcessDestroyer;

import be.pw.jexif.internal.thread.JExifOutputStream;
import be.pw.jexif.internal.thread.event.DebugHandler;

import com.google.common.eventbus.EventBus;

class EchoTest {

	private static Executor executor = null;
	private static final EventBus BUS = new EventBus();
	private static DefaultExecuteResultHandler resultHandler = null;

	/**
	 * @param args
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void main(final String[] args) throws IOException, InterruptedException {
		CommandLine cl = CommandLine.parse("/home/phillip/src/j-exiftool/exiftool/exiftool /home/phillip/src/j-exiftool/src/test/resources/read01.JPG -b -ThumbnailImage"); // TODO: make better use of CommandLine
		resultHandler = new DefaultExecuteResultHandler();
		executor = DefaultExecutor.builder().get();
		executor.setWatchdog(ExecuteWatchdog.builder().setTimeout(Duration.ofMillis(10000L)).get());
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		executor.setStreamHandler(new PumpStreamHandler(bos, new JExifOutputStream(BUS, true)));
		executor.setProcessDestroyer(new ShutdownHookProcessDestroyer());
		BUS.register(new DebugHandler());
		executor.execute(cl, resultHandler);

		while (executor.getWatchdog().isWatching()) {
			Thread.sleep(10);
		}

		bos.writeTo(new FileOutputStream(new File("./bin.jpg")));
	}

}
