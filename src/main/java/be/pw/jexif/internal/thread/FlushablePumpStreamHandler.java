/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.thread;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.exec.PumpStreamHandler;

/**
 * Adds functionality to flush the buffers.
 * 
 * @author phillip
 *
 */
public class FlushablePumpStreamHandler extends PumpStreamHandler {

	public FlushablePumpStreamHandler() {
		super();
	}

	public FlushablePumpStreamHandler(OutputStream out, OutputStream err, InputStream input) {
		super(out, err, input);
	}

	public FlushablePumpStreamHandler(OutputStream out, OutputStream err) {
		super(out, err);
	}

	public FlushablePumpStreamHandler(OutputStream outAndErr) {
		super(outAndErr);
	}

	/**
	 * Flushes the streams.
	 * 
	 * @throws IOException if thrown by {@link PumpStreamHandler}
	 */
	public void flush() throws IOException {
		if (getErr() != null) {
			getErr().flush();
		}
		if (getOut() != null) {
			getOut().flush();
		}
	}

}
