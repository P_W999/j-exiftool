/*******************************************************************************
 * Copyright 2014 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.action.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import be.pw.jexif.enums.Errors;
import be.pw.jexif.exception.JExifException;
import be.pw.jexif.internal.action.IThumbnailAction;
import be.pw.jexif.internal.constants.ExecutionConstant;
import be.pw.jexif.internal.util.ArrayUtil;
import be.pw.jexif.internal.util.Cal10nUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author phillip
 *
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PACKAGE)	// use the factory to create a new instance
public class ThumbnailAction extends AbstractAction implements IThumbnailAction {

	/**
	 * Defines how the thumbnail image will be called.
	 * <p>
	 * Can be either a suffix like '_tn.jpg' or a path (relative or absolute) containing the '%f' placeholder which will be replaced by the original filename.
	 * <p>
	 * If not provided, it will default to _tn + the extention of the input file.
	 */
	private String format;

	/**
	 * Sets the parameter for this action.
	 *
	 * @param file the file from which a thumnail image should be extracted
	 * @param format the thumnail {@link #format}
	 * @throws IOException if the input file does not exist
	 */
	void setParams(final File file, final String format) throws IOException {
		Preconditions.checkNotNull(file);
		if (!file.exists()) {
			throw new FileNotFoundException(Cal10nUtil.get(Errors.IO_FILE_NOT_VALID, file.getAbsolutePath()));
		}
		this.file = file;
		if (Strings.isNullOrEmpty(format)) {
			String[] splitName = file.getName().split("\\.");
			if (splitName.length > 1) {
				this.format = "_tn." + splitName[splitName.length - 1];
			} else {
				this.format = "_tn";
			}
		} else {
			this.format = format;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] buildArguments() throws JExifException {
		try {
			String[] arguments = new String[10];
			int i = 0;
			arguments[i++] = ExecutionConstant.ECHO;
			arguments[i++] = ExecutionConstant.START + " " + getId();
			arguments[i++] = file.getCanonicalPath();
			arguments[i++] = ExecutionConstant.THUMBNAIL;
			arguments[i++] = ExecutionConstant.BINARY;
			arguments[i++] = ExecutionConstant.WRITE;
			arguments[i++] = format;
			arguments[i++] = ExecutionConstant.ECHO;
			arguments[i++] = ExecutionConstant.STOP + " " + getId();
			arguments[i] = ExecutionConstant.EXECUTE;
			log.trace("Arguments are : " + ArrayUtil.toString(arguments));
			return arguments;
		} catch (IOException e) {
			throw new JExifException(Cal10nUtil.get(Errors.IO_BUILD_ARGUMENTS), e);
		}
	}

}
