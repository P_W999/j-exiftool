/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.util;

import lombok.experimental.UtilityClass;

/**
 * The ArrayUtil class is used for centralizing different array operations.
 *
 * @author phillip
 */
@UtilityClass
public final class ArrayUtil {

	/**
	 * Converts an array to a String. Can be usefull for logging purposes.<br />
	 * The output format of an array with 4 objects is [a,b,c,d] (where a, b, c and d are the 4 objects in the array)<br />
	 * The toString() method of the objects is used.
	 *
	 * @param os the array which needs to be converted.
	 * @return the array as String
	 */
	public static String toString(final Object[] os) {
		StringBuilder b = new StringBuilder();
		b.append("[");
		if (os != null) {
			for (Object o : os) {
				b.append(o);
				b.append(",");
			}
		}
		b.deleteCharAt(b.length() - 1);
		b.append("]");
		return b.toString();
	}
}
