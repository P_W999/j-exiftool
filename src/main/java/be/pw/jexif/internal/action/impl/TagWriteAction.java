/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.action.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.base.Preconditions;

import be.pw.jexif.enums.Errors;
import be.pw.jexif.enums.tag.Tag;
import be.pw.jexif.exception.JExifException;
import be.pw.jexif.exception.JExifValidationException;
import be.pw.jexif.internal.action.ITagWriteAction;
import be.pw.jexif.internal.constants.ExecutionConstant;
import be.pw.jexif.internal.util.ArrayUtil;
import be.pw.jexif.internal.util.Cal10nUtil;
import be.pw.jexif.internal.util.TagUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Action to write an Exif-Tag.
 *
 * @author phillip
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PACKAGE)	// use the factory to create a new instance
class TagWriteAction extends AbstractAction implements ITagWriteAction {

	/**
	 * Map with Tag-Value (write Value to Tag).
	 */
	private Map<Tag, String> valuesToWrite;

	/**
	 * Sets the params.
	 *
	 * @param file the file from which you would like to change the Exif tag values.
	 * @param valuesToWrite a Map combining the Tag-Value combination. The value element shall be written to the Tag.<br />
	 *            If you want to clear a Tag, just specify an empty String.
	 * @throws JExifValidationException if the give value is not of a valid type.
	 * @throws IOException if the given file does not exist.
	 * @throws IllegalArgumentException if provided fields are null, empty or if trying to write to a protected/avoided/unsafe tag.
	 */
	void setParams(final File file, final Map<Tag, String> valuesToWrite) throws JExifValidationException, IOException {
		Preconditions.checkNotNull(file);
		if (!file.exists()) {
			throw new FileNotFoundException(Cal10nUtil.get(Errors.IO_FILE_NOT_VALID, file.getAbsolutePath()));
		}
		Preconditions.checkNotNull(valuesToWrite);
		Preconditions.checkArgument(valuesToWrite.size() != 0, Cal10nUtil.get(Errors.VALIDATION_VALUESLIST));
		for (Entry<Tag, String> entry : valuesToWrite.entrySet()) {
			Preconditions.checkArgument(!entry.getKey().isUnsafe(), Cal10nUtil.get(Errors.VALIDATION_WRITE_UNSAFE_TAG, entry.getKey().getName()));
			Preconditions.checkArgument(!entry.getKey().isProtectedField(), Cal10nUtil.get(Errors.VALIDATION_WRITE_PROTECTED_TAG, entry.getKey().getName()));
			Preconditions.checkArgument(!entry.getKey().isAvoided(), Cal10nUtil.get(Errors.VALIDATION_WRITE_AVOIDED_TAG, entry.getKey().getName()));
			TagUtil.validateValue(entry.getKey(), entry.getValue());
		}
		this.file = file;
		this.valuesToWrite = valuesToWrite;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] buildArguments() throws JExifException {
		try {
			int size = valuesToWrite.size() + 2;
			String[] arguments = new String[size];
			int i = 0;
			for (Entry<Tag, String> tag : valuesToWrite.entrySet()) {
				arguments[i++] = "-" + tag.getKey().getName() + "=" + tag.getValue();
			}
			arguments[i++] = ExecutionConstant.EXACT_VALUE;
			arguments[i++] = file.getCanonicalPath();
			log.trace("Arguments are : " + ArrayUtil.toString(arguments));
			return arguments;
		} catch (IOException e) {
			throw new JExifException(Cal10nUtil.get(Errors.IO_BUILD_ARGUMENTS), e);
		}
	}
}
