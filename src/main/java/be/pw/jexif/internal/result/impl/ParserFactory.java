/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.result.impl;

import com.google.common.base.Preconditions;

import be.pw.jexif.enums.Errors;
import be.pw.jexif.exception.JExifException;
import be.pw.jexif.internal.action.IAction;
import be.pw.jexif.internal.action.ICopyFromAction;
import be.pw.jexif.internal.action.IDateTimeShiftAction;
import be.pw.jexif.internal.action.IDeleteAllAction;
import be.pw.jexif.internal.action.ITagReadAction;
import be.pw.jexif.internal.action.ITagReadAllAction;
import be.pw.jexif.internal.action.ITagReadExactAction;
import be.pw.jexif.internal.action.ITagReadExactAllAction;
import be.pw.jexif.internal.action.ITagWriteAction;
import be.pw.jexif.internal.action.IThumbnailAction;
import be.pw.jexif.internal.result.IResultParser;
import be.pw.jexif.internal.util.Cal10nUtil;
import lombok.experimental.UtilityClass;

/**
 * Factory for creating the different parsers.
 *
 * @author phillip
 * @see be.pw.jexif.internal.result.IResultParser
 */
@UtilityClass
public class ParserFactory {

	/**
	 * Returns an instance of a parser for a given action instance.
	 *
	 * @param action the action for which you want a parser.
	 * @return the instance of a parser
	 * @throws JExifException if the action is null or if an unknown action type is given.
	 */
	public static IResultParser getInstance(final IAction action) throws JExifException {
		Preconditions.checkNotNull(action);
		if (action instanceof ITagReadAction) {
			return new TagReadParser();
		}
		if (action instanceof ITagReadExactAction) {
			return new TagReadExactParser();
		}
		if (action instanceof ITagWriteAction) {
			return new TagWriteParser();
		}
		if (action instanceof ITagReadAllAction) {
			return new TagReadAllParser();
		}
		if (action instanceof ITagReadExactAllAction) {
			return new TagReadExactAllParser();
		}
		if (action instanceof IDateTimeShiftAction) {
			return new DateTimeShiftParser();
		}
		if (action instanceof ICopyFromAction) {
			return new CopyFromResultParser();
		}
		if (action instanceof IThumbnailAction) {
			return new ThumnailActionParser();
		}
		if (action instanceof IDeleteAllAction) {
			return new DeleteAllParser();
		}
		throw new JExifException(Cal10nUtil.get(Errors.PARSER_UNKNOWNTYPE, action.getClass().getName()));
	}
}
