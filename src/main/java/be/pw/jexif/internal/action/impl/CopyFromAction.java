/*******************************************************************************
 * Copyright 2013 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.action.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;

import com.google.common.base.Preconditions;

import be.pw.jexif.enums.Errors;
import be.pw.jexif.enums.tag.Tag;
import be.pw.jexif.exception.JExifException;
import be.pw.jexif.exception.JExifValidationException;
import be.pw.jexif.internal.action.ICopyFromAction;
import be.pw.jexif.internal.constants.ExecutionConstant;
import be.pw.jexif.internal.util.ArrayUtil;
import be.pw.jexif.internal.util.Cal10nUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Action to perform date/time-shifts.
 *
 * @author phillip
 * @since v0.0.6
 *
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PACKAGE)	// use the factory to create a new instance
class CopyFromAction extends AbstractAction implements ICopyFromAction {

	/**
	 * Contains all the tags that must be copied. Empty collections are allowed.
	 */
	private Collection<Tag> valuesToCopy;

	/**
	 * The file from which the tag values are read.
	 */
	private File sourceFile;

	/**
	 * Sets the params.
	 *
	 * @param from the file from which the tag values will be extracted
	 * @param to the file to which the tag values will be writen
	 * @param valuesToCopy list containing the tag values to copy
	 * @throws JExifValidationException if the give value is not of a valid type.
	 * @throws IOException if the given file does not exist.
	 * @throws IllegalArgumentException if provided fields are null, empty or if trying to write to a protected/avoided/unsafe tag.
	 */
	void setParams(final File from, final File to, final Collection<Tag> valuesToCopy) throws JExifValidationException, IOException {
		Preconditions.checkNotNull(from);
		if (!from.exists()) {
			throw new FileNotFoundException(Cal10nUtil.get(Errors.IO_FILE_NOT_VALID, from.getAbsolutePath()));
		}
		Preconditions.checkNotNull(to);
		if (!to.exists()) {
			throw new FileNotFoundException(Cal10nUtil.get(Errors.IO_FILE_NOT_VALID, to.getAbsolutePath()));
		}
		if (valuesToCopy != null && !valuesToCopy.isEmpty()) {
			for (Tag tag : valuesToCopy) {
				Preconditions.checkArgument(!tag.isUnsafe(), Cal10nUtil.get(Errors.VALIDATION_WRITE_UNSAFE_TAG, tag.getName()));
				Preconditions.checkArgument(!tag.isProtectedField(), Cal10nUtil.get(Errors.VALIDATION_WRITE_PROTECTED_TAG, tag.getName()));
				Preconditions.checkArgument(!tag.isAvoided(), Cal10nUtil.get(Errors.VALIDATION_WRITE_AVOIDED_TAG, tag.getName()));
			}
		}
		this.file = to;
		this.sourceFile = from;
		this.valuesToCopy = valuesToCopy;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] buildArguments() throws JExifException {
		try {
			int size = valuesToCopy.size() + 3;
			String[] arguments = new String[size];
			int i = 0;
			arguments[i++] = ExecutionConstant.TAGSFROMFILE;
			arguments[i++] = sourceFile.getCanonicalPath();
			for (Tag tag : valuesToCopy) {
				arguments[i++] = "-" + tag.getName();
			}
			arguments[i] = file.getCanonicalPath();
			log.trace("Arguments are : " + ArrayUtil.toString(arguments));
			return arguments;
		} catch (IOException e) {
			throw new JExifException(Cal10nUtil.get(Errors.IO_BUILD_ARGUMENTS), e);
		}
	}

}
