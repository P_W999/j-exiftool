/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.action.impl;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import be.pw.jexif.enums.tag.Tag;
import be.pw.jexif.internal.action.IAction;
import lombok.extern.slf4j.Slf4j;

/**
 * An abstract implementation of the IAction interface.
 *
 * @author phillip
 */
@Slf4j
abstract class AbstractAction implements IAction {

	protected File file = null;
	protected Set<Tag> tags = null;
	private final Map<Tag, String> results;
	private Map<String, String> unsupportedTags;
	private UUID id = null;

	protected AbstractAction() {
		this.tags = new HashSet<>();
		this.results = new HashMap<>();
		this.unsupportedTags = new HashMap<>();
		this.id = UUID.randomUUID();
		log.trace("New action created with UUID {}", id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getId() {
		return id.toString();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<Tag, String> getResult() {
		return results;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addResult(final Tag tag, final String result) {
		results.put(tag, result);
	}

	@Override
	public void addUnsupportedResult(final String tag, final String result) {
		unsupportedTags.put(tag, result);
	}

	/**
	 * @return the unsupportedTags
	 */
	@Override
	public final Map<String, String> getUnsupportedTags() {
		return unsupportedTags;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public File getFile() {
		return file;
	}
}
