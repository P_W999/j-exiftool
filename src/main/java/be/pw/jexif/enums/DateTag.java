/*******************************************************************************
 * Copyright 2013 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.enums;

import be.pw.jexif.enums.tag.ExifIFD;
import be.pw.jexif.enums.tag.IFD0;
import be.pw.jexif.enums.tag.Tag;

/**
 * This enumeration contains a list of Tags which can be timeshifted.
 * <p>
 * The ALLDATES tag is an ExifTool shortcut for:
 * <ul>
 * <li>DateTimeOriginal
 * <li>CreateDate
 * <li>ModifyDate
 * </ul>
 * See also {@link be.pw.jexif.JExifInfo#timeShift(DateTag, String)}
 *
 * @author phillip
 * @since v0.0.6
 */
public enum DateTag {

	DATETIMEORIGINAL(ExifIFD.DATETIMEORIGINAL), CREATEDATE(ExifIFD.CREATEDATE), MODIFYDATE(IFD0.MODIFYDATE), ALLDATES(null);

	/**
	 * The tag it represents.
	 */
	private Tag tag;

	/**
	 * Hidden constructor.
	 *
	 * @param tag the tag definition
	 */
	private DateTag(final Tag tag) {
		this.tag = tag;
	}

	/**
	 * Returns the Tag representation of a date/time value.
	 *
	 * @return a tag
	 */
	public Tag getTag() {
		return this.tag;
	}
}
