/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.constants;

import lombok.experimental.UtilityClass;

/**
 * Some constants used before, during and after the execution of ExifTool.<br />
 * For the different Tags, see {@link be.pw.jexif.enums.tag}
 *
 * @author phillip
 */
@UtilityClass
public final class ExecutionConstant {


	/**
	 * Echo parameter to indicate that an action was started.
	 */
	public static final String START = "**START**";
	/**
	 * Echo parameter to indicate that an action was finished.
	 */
	public static final String STOP = "**STOP**";
	/**
	 * Strings that ExifTool outputs to indicate that it has finished it's execution.
	 */
	public static final String EXIFTOOLREADY = "{ready}";
	/**
	 * System property key for defining the path to Exif tool.
	 */
	public static final String EXIFTOOLPATH = "exiftool.path";

	/**
	 * System environment variable where exiftool is located.
	 */
	public static final String EXIFTOOLENV = "EXIFTOOL_PATH";

	/**
	 * System property key for the deadlock-time in milliseconds.
	 */
	public static final String EXIFTOOLDEADLOCK = "exiftool.deadlock";

	/**
	 * System property key for bypassing the validation of Tags.
	 */
	public static final String EXIFTOOLBYPASSVALIDATION = "exiftool.validation.bypass";

	/**
	 * System property key for setting the encoding used by the {@link be.pw.jexif.internal.thread.JExifOutputStream}.
	 *
	 * @since 0.0.7
	 */
	public static final String EXIFTOOLCLIENCODING = "exiftool.cli.encoding";

	/**
	 * Command line argument to keep ExifTool open.
	 */
	public static final String STAY_OPEN = "-stay_open";

	/**
	 * Command line argument to let ExifTool print a provided line of text.
	 */
	public static final String ECHO = "-echo";

	/**
	 * 1024 characters to forcefully flush the pumpstream buffer.
	 */
	public static final String FLUSH = "----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n";

	/**
	 * Command line argument to have ExifTool output the exact values.
	 */
	public static final String EXACT_VALUE = "-n";

	/**
	 * Command line argument to have ExifTool start processing the provided arguments.
	 */
	public static final String EXECUTE = "-execute";

	/**
	 * Command line argument to have ExifTool copy tags from a file to a file.
	 */
	public static final String TAGSFROMFILE = "-tagsFromFile";

	/**
	 * Command line argument to have exitfool extract a thumbnail image from a file.
	 *
	 * @since v0.0.8
	 */
	public static final String THUMBNAIL = "-ThumbnailImage";

	/**
	 * Command line argument to have exiftool extract the data in binary form.
	 *
	 * @since v0.0.8
	 */
	public static final String BINARY = "-b";

	/**
	 * Command line argument to have exiftool write the binary output to a file.
	 *
	 * @since v0.0.8
	 */
	public static final String WRITE = "-w";

	/**
	 * Command line argument to reference all known exif tags.
	 *
	 * @since v0.0.8
	 */
	public static final String ALLTAGS = "-exif:all";

	/**
	 * Command line argument to have ExifTool output it's data in short format.
	 */
	public static final String SHORT_FORMAT = "-S";
	/**
	 * Command line argument to start ExifTool on a Windows based computer.
	 */
	public static final String WINDOWS_CLA = "\"{0}\" " + STAY_OPEN + " true -@ \"{1}\"";

	/**
	 * Command line argument to start ExifTool on a Linux based computer.
	 */
	public static final String LINUX_CLA = "{0} " + STAY_OPEN + " true -@ {1}";

	/**
	 * Command line argument to start ExifTool on a Mac based computer.
	 */
	public static final String MAC_CLA = "{0} " + STAY_OPEN + " true -@ \"{1}\"";

	/**
	 * A list of ExifTool warnings that should be ignored by the result parser.
	 */
	public static final String[] WARNINGS_TO_IGNORE = {};

}
