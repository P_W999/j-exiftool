/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.enums.tag;

import be.pw.jexif.enums.DataType;

/**
 * This is the definition of an Exif tag. For each group of tags, an enumeration which implements this interface should be created.<br />
 * Implementations of this interface are based on the info found on the website of ExifTool: <a href="http://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/index.html">http://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/index.html</a>
 */
public interface Tag {

	/**
	 * An avoided tags is a tag that is not created when writing if another same-named tag may be created instead.
	 *
	 * @return true if the tag is avoided.
	 */
	boolean isAvoided();

	/**
	 * Indicates a tag that is considered unsafe to write under normal circumstances.
	 *
	 * @return true if the tag is unsafe.
	 */
	boolean isUnsafe();

	/**
	 * Indicates a protected tag which is not writable directly, but is written automatically by ExifTool (often when a corresponding Composite or Extra tag is written).
	 *
	 * @return true if the tag is protected.
	 */
	boolean isProtectedField();

	/**
	 * @return the name of the tag (as it should be given on the command line)
	 */
	String getName();

	/**
	 * @return the data type of the tag
	 */
	DataType getType();

}
