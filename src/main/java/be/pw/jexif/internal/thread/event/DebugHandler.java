/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.thread.event;

import com.google.common.eventbus.Subscribe;

import lombok.extern.slf4j.Slf4j;

/**
 * The DebugHandler class can be used to more easily debug the output from ExifTool.<br />
 * {@link InputEvent} will be logged as an debug-message. {@link ErrorEvent} will be logged as an error-message.
 *
 * @author phillip
 */
@Slf4j
public class DebugHandler {

	/**
	 * Logs the InputEvent to the logger's debug stream.
	 *
	 * @param input the input-event.
	 */
	@Subscribe
	public void handle(final InputEvent input) {
		log.debug(input.getMsg());
	}

	/**
	 * Logs the ErrorEvent to the logger's error stream.
	 *
	 * @param error the error-event.
	 */
	@Subscribe
	public void handle(final ErrorEvent error) {
		log.error(error.getMsg());
	}

}
