/*******************************************************************************
 * Copyright 2014 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.result.impl;

import java.util.List;

import be.pw.jexif.internal.action.IAction;
import be.pw.jexif.internal.action.IDeleteAllAction;
import be.pw.jexif.internal.constants.ExecutionConstant;
import be.pw.jexif.internal.result.IResultParser;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@NoArgsConstructor(access = AccessLevel.PACKAGE)	// use the factory to create a new instance
class DeleteAllParser implements IResultParser {

	@Override
	public void parse(final IAction action, final String actionUID, final List<String> result) {
		for (String line : result) {
			if (!line.contains(ExecutionConstant.EXIFTOOLREADY) && !line.isEmpty()) {
				log.trace(line);
			}
		}
	}

	@Override
	public Class<? extends IAction> validFor() {
		return IDeleteAllAction.class;
	}

}
