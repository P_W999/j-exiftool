/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.action.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;

import com.google.common.base.Preconditions;

import be.pw.jexif.enums.Errors;
import be.pw.jexif.enums.tag.Tag;
import be.pw.jexif.exception.JExifException;
import be.pw.jexif.internal.action.ITagReadAction;
import be.pw.jexif.internal.constants.ExecutionConstant;
import be.pw.jexif.internal.util.ArrayUtil;
import be.pw.jexif.internal.util.Cal10nUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Action to read an EXIF-tag.
 *
 * @author phillip
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PACKAGE)	// use the factory to create a new instance
class TagReadAction extends AbstractAction implements ITagReadAction {

	/**
	 * Allows settings the required parameters. AnnotationValidation is possible.
	 *
	 * @param file the file from which the provided Tags should be read.
	 * @param tags the Tags that must be read from the provided file.
	 * @throws IOException if the given file does not exist.
	 */
	void setParams(final File file, final Tag... tags) throws IOException {
		Preconditions.checkNotNull(file);
		if (!file.exists()) {
			throw new FileNotFoundException(Cal10nUtil.get(Errors.IO_FILE_NOT_VALID, file.getAbsolutePath()));
		}
		Preconditions.checkNotNull(tags);
		Preconditions.checkArgument(tags.length != 0, Cal10nUtil.get(Errors.VALIDATION_TAGLIST));
		this.file = file;
		this.tags = new HashSet<>();
		for (Tag tag : tags) {
			log.trace("Adding tag {}", tag.getName());
			this.tags.add(tag);
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @throws JExifException in case of an IO-Exception.
	 */
	@Override
	public String[] buildArguments() throws JExifException {
		try {
			int size = tags.size() + 2;
			String[] arguments = new String[size];
			int i = 0;
			for (Tag tag : tags) {
				arguments[i++] = "-" + tag.getName();
			}
			arguments[i++] = ExecutionConstant.SHORT_FORMAT;
			arguments[i++] = file.getCanonicalPath();
			log.trace("Arguments are : " + ArrayUtil.toString(arguments));
			return arguments;
		} catch (IOException e) {
			throw new JExifException(Cal10nUtil.get(Errors.IO_BUILD_ARGUMENTS), e);
		}
	}
}
