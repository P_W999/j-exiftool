/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.enums.tag;

import static be.pw.jexif.enums.DataType.INT16U;
import static be.pw.jexif.enums.DataType.INT8U;
import static be.pw.jexif.enums.DataType.RAT64U;
import static be.pw.jexif.enums.DataType.STRING;
import static be.pw.jexif.enums.DataType.UNDEF;
import be.pw.jexif.enums.DataType;
import lombok.Getter;

/**
 * Implementation of the GPS group: <a href="http://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/GPS.html">http://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/GPS.html</a> .
 *
 * @author phillip
 */
@Getter
public enum ExifGPS implements Tag {
	GPSVERSIONID("GPSVersionID", false, false, false, INT8U), GPSLATITUDEREF("GPSLatitudeRef", false, false, false, STRING), GPSLATITUDE("GPSLatitude", false, false, false, STRING), GPSLONGITUDEREF("GPSLongitudeRef", false, false, false, STRING), GPSLONGITUDE("GPSLongitude", false, false, false, STRING), GPSALTITUDEREF("GPSAltitudeRef", false, false, false, INT8U), GPSALTITUDE("GPSAltitude", false, false, false, RAT64U), GPSTIMESTAMP("GPSTimeStamp", false, false, false, STRING), GPSSATELLITES("GPSSatellites", false, false, false, STRING), GPSSTATUS("GPSStatus", false, false, false, STRING), GPSMEASUREMODE("GPSMeasureMode", false, false, false, STRING), GPSDOP("GPSDOP", false, false, false, RAT64U), GPSSPEEDREF("GPSSpeedRef", false, false, false, STRING), GPSSPEED("GPSSpeed", false, false, false, RAT64U), GPSTRACKREF("GPSTrackRef", false, false, false, STRING), GPSTRACK("GPSTrack", false, false, false, RAT64U), GPSIMGDIRECTIONREF("GPSImgDirectionRef", false, false, false, STRING), GPSIMGDIRECTION("GPSImgDirection", false, false, false, RAT64U), GPSMAPDATUM("GPSMapDatum", false, false, false, STRING), GPSDESTLATITUDEREF("GPSDestLatitudeRef", false, false, false, STRING), GPSDESTLATITUDE("GPSDestLatitude", false, false, false, RAT64U), GPSDESTLONGITUDEREF("GPSDestLongitudeRef", false, false, false, STRING), GPSDESTLONGITUDE("GPSDestLongitude", false, false, false, RAT64U), GPSDESTBEARINGREF("GPSDestBearingRef", false, false, false, STRING), GPSDESTBEARING("GPSDestBearing", false, false, false, RAT64U), GPSDESTDISTANCEREF("GPSDestDistanceRef", false, false, false, STRING), GPSDESTDISTANCE("GPSDestDistance", false, false, false, RAT64U), GPSPROCESSINGMETHOD("GPSProcessingMethod", false, false, false, UNDEF), GPSAREAINFORMATION("GPSAreaInformation", false, false, false, UNDEF), GPSDATESTAMP("GPSDateStamp", false, false, false, STRING), GPSDIFFERENTIAL("GPSDifferential", false, false, false, INT16U), GPSHPOSITIONINGERROR("GPSHPositioningError", false, false, false, RAT64U);

	private final boolean avoided;
	private final boolean unsafe;
	private final boolean protectedField;
	private final String name;
	private final DataType type;

	/**
	 * @param avoided indicate that the tag is avoided
	 * @param unsafe indicated that the tag is unsafe
	 * @param protectedField indicate that the tag is protected
	 * @param name the name of the tag
	 * @param type the data type of the tag
	 */
	private ExifGPS(final String name, final boolean unsafe, final boolean avoided, final boolean protectedField, final DataType type) {
		this.avoided = avoided;
		this.unsafe = unsafe;
		this.protectedField = protectedField;
		this.name = name;
		this.type = type;
	}

}
