/*******************************************************************************
 * Copyright 2014 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.action.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.google.common.base.Preconditions;

import be.pw.jexif.enums.Errors;
import be.pw.jexif.exception.JExifException;
import be.pw.jexif.internal.action.IDeleteAllAction;
import be.pw.jexif.internal.constants.ExecutionConstant;
import be.pw.jexif.internal.util.ArrayUtil;
import be.pw.jexif.internal.util.Cal10nUtil;
import lombok.extern.slf4j.Slf4j;
@Slf4j
class DeleteAllAction extends AbstractAction implements IDeleteAllAction {


	DeleteAllAction() {
		super();
	}

	void setParams(final File file) throws FileNotFoundException {
		Preconditions.checkNotNull(file);
		if (!file.exists()) {
			throw new FileNotFoundException(Cal10nUtil.get(Errors.IO_FILE_NOT_VALID, file.getAbsolutePath()));
		}
		this.file = file;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] buildArguments() throws JExifException {
		try {
			String[] arguments = new String[3];
			int i = 0;
			arguments[i++] = file.getCanonicalPath();
			arguments[i++] = ExecutionConstant.ALLTAGS + "=";
			arguments[i++] = ExecutionConstant.EXECUTE;
			log.trace("Arguments are : " + ArrayUtil.toString(arguments));
			return arguments;
		} catch (IOException e) {
			throw new JExifException(Cal10nUtil.get(Errors.IO_BUILD_ARGUMENTS), e);
		}
	}

}
