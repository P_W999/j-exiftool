/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.action.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

import be.pw.jexif.enums.DateTag;
import be.pw.jexif.enums.tag.Tag;
import be.pw.jexif.exception.JExifValidationException;
import be.pw.jexif.internal.action.ICopyFromAction;
import be.pw.jexif.internal.action.IDateTimeShiftAction;
import be.pw.jexif.internal.action.IDeleteAllAction;
import be.pw.jexif.internal.action.ITagReadAction;
import be.pw.jexif.internal.action.ITagReadAllAction;
import be.pw.jexif.internal.action.ITagReadExactAllAction;
import be.pw.jexif.internal.action.ITagWriteAction;
import be.pw.jexif.internal.action.IThumbnailAction;
import lombok.experimental.UtilityClass;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * Factory for the different action.
 *
 * @see be.pw.jexif.internal.action.IAction
 */
@UtilityClass
public final class ActionFactory {

	/**
	 * Creates a new instance of a {@link TagReadAction}<br />
	 * TagReadActions can be used to retrieve one or multiple Tags from a single image.
	 *
	 * @param file the file from which the tags should be retrieved. May not be null.
	 * @param tags the tags that should be read. May not be null.
	 * @return an instance of a TagReadAction or null if validation failed.
	 * @throws IOException if the given file does not exist
	 */
	public static ITagReadAction createReadAction(final File file, final Tag... tags) throws IOException {
		TagReadAction read = new TagReadAction();
		read.setParams(file, tags);
		return read;
	}

	/**
	 * Creates a new instance of a {@link TagReadExactAction}<br />
	 * TagReadExactAction can be used to retrieve the exact value of one or multiple Tags from a single image.
	 *
	 * @param file the file from which the tags should be retrieved. May not be null.
	 * @param tags the tags that should be read. May not be null.
	 * @return an instance of a TagReadAction or null if validation failed.
	 * @throws IOException if the given file does not exist
	 */
	public static ITagReadAction createExactReadAction(final File file, final Tag... tags) throws IOException {
		TagReadExactAction read = new TagReadExactAction();
		read.setParams(file, tags);
		return read;
	}

	public static ITagWriteAction createTagWriteAction(final File file, final Map<Tag, String> valuesToWrite) throws JExifValidationException, IOException {
		TagWriteAction read = new TagWriteAction();
		read.setParams(file, valuesToWrite);
		return read;
	}

	public static ITagReadAllAction createTagReadAllAction(final File file) {
		TagReadAllAction readAll = new TagReadAllAction();
		readAll.setParams(file);
		return readAll;
	}

	public static ITagReadExactAllAction createTagReadExactAllAction(final File file) {
		TagReadExactAllAction readExactAll = new TagReadExactAllAction();
		readExactAll.setParams(file);
		return readExactAll;
	}

	public static IDateTimeShiftAction createDateTimeShiftAction(final File file, final String shift, final DateTag... tags) throws IOException {
		DateTimeShiftAction dateTimeShift = new DateTimeShiftAction();
		dateTimeShift.setParams(file, shift, Sets.newHashSet(tags));
		return dateTimeShift;
	}

	public static ICopyFromAction createCopyFromAction(final File from, final File to, final Tag... tags) throws IOException, JExifValidationException {
		CopyFromAction copyFromAction = new CopyFromAction();
		copyFromAction.setParams(from, to, Lists.newArrayList(tags));
		return copyFromAction;
	}

	public static IThumbnailAction createThumnailAction(final File file, final String format) throws JExifValidationException, IOException {
		ThumbnailAction thumbnailAction = new ThumbnailAction();
		thumbnailAction.setParams(file, format);
		return thumbnailAction;
	}

	public static IDeleteAllAction createDeleteAllAction(final File file) throws FileNotFoundException {
		DeleteAllAction deleteAllAction = new DeleteAllAction();
		deleteAllAction.setParams(file);
		return deleteAllAction;
	}
}
