/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.result.impl;

import java.util.Iterator;
import java.util.List;

import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;

import be.pw.jexif.enums.Errors;
import be.pw.jexif.enums.tag.Tag;
import be.pw.jexif.internal.action.IAction;
import be.pw.jexif.internal.action.ITagReadAllAction;
import be.pw.jexif.internal.constants.ExecutionConstant;
import be.pw.jexif.internal.result.IResultParser;
import be.pw.jexif.internal.util.Cal10nUtil;
import be.pw.jexif.internal.util.TagUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Parses the result for the {@link TagReadAllAction}.
 *
 * @author phillip
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PACKAGE)	// use the factory to create a new instance
class TagReadAllParser implements IResultParser {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void parse(final IAction action, final String actionUID, final List<String> result) {
		Preconditions.checkArgument(validFor().isAssignableFrom(action.getClass()), Cal10nUtil.get(Errors.PARSER_MISMATCH, getClass().getSimpleName(), action.getClass().getSimpleName()));
		Preconditions.checkArgument(action.getId().equals(actionUID), "Invalid action UID");

		Tag t;
		String r;
		for (String line : result) {
			if (!line.contains(ExecutionConstant.EXIFTOOLREADY) && !line.isEmpty() && line.contains(":")) {
				Iterable<String> output = Splitter.on(":").limit(2).omitEmptyStrings().trimResults().split(line);
				Iterator<String> i = output.iterator();
				String u = i.next();
				t = TagUtil.forName(u);
				if (i.hasNext()) { // result exists
					r = i.next();
				} else {
					r = null;
				}
				if (i.hasNext()) {
					log.warn("More values exist in the iterator for line: {}", line);
				}
				if (t != null && r != null && !r.isEmpty()) {
					log.trace("Found results {} for Tag {}", r, t.getName());
					action.addResult(t, r);
				} else {
					if (t == null) {
						log.trace("Found unsupported tag: {}", u);
						((ITagReadAllAction) action).addUnsupportedResult(u, r);
					} else {
						log.warn("Could not add result {}", line);
					}
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<? extends IAction> validFor() {
		return ITagReadAllAction.class;
	}

}
