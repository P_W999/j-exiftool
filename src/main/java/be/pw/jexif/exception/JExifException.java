/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.exception;

/**
 * The standard exception. This can be used for anything.<br />
 * The goal is to launch only one exception, this makes exception handling far more easy.<br />
 * This may not be the best strategy, but if it fails, it fails.
 *
 * @author phillip
 */
public class JExifException extends Exception {

	/**
	 * UID.
	 */
	private static final long serialVersionUID = 1768965397370932970L;

	/**
	 * Hidden constructor to prevent creation of JExifExceptions without information.
	 */
	protected JExifException() {
		super();
	}

	/**
	 * @param message the reason of the exception or a clear message for the user
	 * @param cause the cause
	 */
	public JExifException(final String message, final Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message the reason of the exception or a clear message for the user
	 */
	public JExifException(final String message) {
		super(message);
	}

	/**
	 * @param cause the cause of the exception
	 */
	public JExifException(final Throwable cause) {
		super(cause);
	}
}
