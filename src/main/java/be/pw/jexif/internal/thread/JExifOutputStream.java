/*******************************************************************************
 * Copyright 2013 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.thread;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;

import com.google.common.eventbus.EventBus;

import org.slf4j.LoggerFactory;

import be.pw.jexif.internal.constants.ExecutionConstant;
import be.pw.jexif.internal.thread.event.ErrorEvent;
import be.pw.jexif.internal.thread.event.InputEvent;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JExifOutputStream extends OutputStream {

	private EventBus bus;
	private boolean error;
	private String globalBuff = "";

	public JExifOutputStream(final EventBus bus, final boolean error) throws IOException {
		this.bus = bus;
		this.error = error;
	}

	@Override
	public void write(final int b) throws IOException {

	}

	@Override
	public void write(final byte[] b, final int off, final int len) throws IOException {
		try {
			String line = new String(Arrays.copyOfRange(b, off, len), System.getProperty(ExecutionConstant.EXIFTOOLCLIENCODING, "UTF-8"));
			String[] lines = line.split("[\\r|\\n]");
			for (String event : lines) {
				if (!line.endsWith("\r") && !line.endsWith("\n") && lines[lines.length - 1].equals(event)) {
					log.trace("Data split up over two lines: " + event);
					this.globalBuff = event;
				} else {
					final String buff = globalBuff;
					globalBuff = "";
					String withBuffered = buff + event.trim();
					if (withBuffered.contains(ExecutionConstant.START) && !withBuffered.startsWith(ExecutionConstant.START)) { // flush + start command
						log.trace("Data contains start, but not at beginning" + withBuffered);
						int ix = withBuffered.indexOf("*");
						withBuffered = withBuffered.substring(ix);
					} else if (withBuffered.contains(ExecutionConstant.STOP) && !withBuffered.startsWith(ExecutionConstant.STOP)) { // flush + start command
						log.trace("Data contains stop, but not at end" + withBuffered);
						int ix = withBuffered.indexOf("*");
						withBuffered = withBuffered.substring(ix);
					} else if (withBuffered.startsWith("-") && withBuffered.contains("*") && withBuffered.matches("-{700}")) { // mostly buffer + partial start command
						log.trace("Data contains tag, but mostly -" + withBuffered);
						int ix = withBuffered.indexOf("*");
						withBuffered = withBuffered.substring(ix);
					} else {
						log.trace("No special case for " + withBuffered);
					}

					log.trace("Posting " + withBuffered);
					if (error) {
						bus.post(new ErrorEvent(withBuffered));
					} else {
						bus.post(new InputEvent(withBuffered));
					}
					if (log.isTraceEnabled()) {
						log.trace("Buffer={}", buff);
						log.trace("WitBuffer={}", withBuffered);
						log.trace("Event={}", event);
					}
				}
			}
		} catch (Exception e) {
			LoggerFactory.getLogger(getClass()).error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * Flushes the buffer.
	 */
	@Override
	public void flush() throws IOException {
		final String localBuff = globalBuff;
		globalBuff = "";
		log.trace("Flushing " + localBuff);
		if (error) {
			bus.post(new ErrorEvent(localBuff));
		} else {
			bus.post(new InputEvent(localBuff));
		}
	}

}
