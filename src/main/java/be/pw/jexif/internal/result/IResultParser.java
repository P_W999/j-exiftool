/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.result;

import java.util.List;

import be.pw.jexif.internal.action.IAction;

/**
 * Interface for Classes that will be responsible for parsing the output of an {@link be.pw.jexif.internal.action.IAction}. There should be an IResultParser per IAction.
 */
public interface IResultParser {
	/**
	 * Parses the result.
	 *
	 * @param action the action where the results will be stored.
	 * @param actionUID the UID of the action.
	 * @param result the list with the output of ExifTool. Each line printed by ExifTool should represent a single String in the list.
	 */
	void parse(final IAction action, final String actionUID, final List<String> result);

	/**
	 * Returns the {@link IAction} for which this parser can be used.
	 *
	 * @return the IAction class.
	 */
	Class<? extends IAction> validFor();
}
