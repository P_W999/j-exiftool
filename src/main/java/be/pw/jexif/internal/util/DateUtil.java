/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.experimental.UtilityClass;

/**
 * Generaly utility class for working with dates.
 */
@UtilityClass
public final class DateUtil {

	/**
	 * Generates a "yyyy:MM:dd HH:mm:ss.SS XXX" formatted String based on the given date.
	 *
	 * @param date the given date
	 * @return the formatted String.
	 */
	public static String fromDate(final Date date) {
		return new SimpleDateFormat("yyyy:MM:dd HH:mm:ss.SS XXX").format(date); // not ideal, but see FindBugs bugs STCAL_INVOKE_ON_STATIC_DATE_FORMAT_INSTANCE
	}

	/**
	 * Parses a date from the "yyyy:MM:dd HH:mm:ss.SS XXX" format to a Date.
	 *
	 * @param date the "yyyy:MM:dd HH:mm:ss.SS XXX" formatted date
	 * @return the given date as a java.util.Date object
	 * @throws ParseException if the given date does not have the "yyyy:MM:dd HH:mm:ss.SS XXX" format.
	 */
	public static Date toDate(final String date) throws ParseException {
		return new SimpleDateFormat("yyyy:MM:dd HH:mm:ss.SS XXX").parse(date);
	}
}
