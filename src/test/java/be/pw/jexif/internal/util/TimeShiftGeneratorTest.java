/*******************************************************************************
 * Copyright 2013 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.util;

import static org.fest.assertions.Assertions.assertThat;

import org.testng.annotations.Test;

public class TimeShiftGeneratorTest {

	@Test
	public void testBase() {
		assertThat(TimeShiftGenerator.base(1)).as("Positive value should remain positive").isEqualTo(1);
		assertThat(TimeShiftGenerator.base(-1)).as("Negative value should become positive").isEqualTo(1);
		assertThat(TimeShiftGenerator.base(0)).as("0 should remain untouched").isEqualTo(0);
	}

	@Test
	public void generateTimeShiftbooleanintintint() {
		assertThat(TimeShiftGenerator.generateTimeShift(true, 2, 3, 4)).as("Generated dateshift value should match").isEqualTo("+2:3:4");
		assertThat(TimeShiftGenerator.generateTimeShift(false, 2, 3, 4)).as("Generated dateshift value should match").isEqualTo("-2:3:4");
	}

	@Test
	public void generateTimeShiftbooleanintintintintintintint() {
		assertThat(TimeShiftGenerator.generateTimeShift(true, 1, 2, 3, 4, 5, 6, 7)).as("Generate date/time shift value should match").isEqualTo("+1:2:3 4:5:6:+7");
		assertThat(TimeShiftGenerator.generateTimeShift(false, 1, 2, 3, 4, 5, 6, 7)).as("Generate date/time shift value should match").isEqualTo("-1:2:3 4:5:6:+7");
		assertThat(TimeShiftGenerator.generateTimeShift(false, 1, 2, 3, 4, 5, 6, -7)).as("Generate date/time shift value should match").isEqualTo("-1:2:3 4:5:6:-7");
	}
}
