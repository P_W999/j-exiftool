/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import be.pw.jexif.enums.DateTag;
import be.pw.jexif.enums.tag.ExifGPS;
import be.pw.jexif.enums.tag.Tag;
import be.pw.jexif.exception.ExifError;
import be.pw.jexif.exception.JExifException;

import com.google.common.annotations.Beta;

/**
 * The JExifInfo class can be used to read or write a Tag from and to a given file.<br />
 * Reading tags involves lazy-loading, this means that tag information will only be read when asked for.
 *
 * @author phillip
 */
@Beta
public class JExifInfo {
	/**
	 * The reference to exifTool (and thus: the ExifTool process and EventBus).
	 */
	private final JExifTool exifTool;
	/**
	 * This image file.
	 */
	private final File file;

	/**
	 * Packaged constructor as this object must always be instantiated from an JExifTool instance.
	 *
	 * @param tool the reference to the JExifTool which instantiated this object.
	 * @param imageFile the image file specified by the user.
	 */
	JExifInfo(final JExifTool tool, final File imageFile) {
		this.exifTool = tool;
		this.file = imageFile;
	}

	/**
	 * Reads out a tag.<br />
	 * Reading involves lazy-loading so there might be a little delay when calling this method.
	 *
	 * @param tag the tag to read.
	 * @return the tag value as a String or null if no value found.
	 * @throws JExifException in case something goes wrong internally.
	 * @throws ExifError if there was a problem in ExifTool
	 */
	@Beta
	public String getTagValue(final Tag tag) throws JExifException, ExifError {
		return exifTool.readTagInfo(file, tag);
	}

	/**
	 * Reads out a tag returning it's exact value (so instead of 1/400s it'll return 0.025).<br />
	 * Reading involves lazy-loading so there might be a little delay when calling this method.
	 *
	 * @param tag the tag to read.
	 * @return the tag value as a String or null if no value found.
	 * @throws JExifException in case something goes wrong internally.
	 * @throws ExifError if something went wrong in ExifTool
	 */
	@Beta
	public String getExactTagValue(final Tag tag) throws JExifException, ExifError {
		return exifTool.readTagInfo(file, tag, true);
	}

	/**
	 * Writes a single value to an Exif tag. <br />
	 * Be aware that not all tags are writable. The following methods should all return false if you can safely write something to an Exif tag.
	 * <ul>
	 * <li>{@link Tag#isAvoided()}</li>
	 * <li>{@link Tag#isProtectedField()}</li>
	 * <li>{@link Tag#isUnsafe()}</li>
	 * </ul>
	 *
	 * @param tag the tag to write to.
	 * @param value the value to write.
	 * @return the exact value that was read from the image after writing it.
	 * @throws JExifException in case something goes wrong internally.
	 * @throws ExifError if something went wrong in ExifTool
	 */
	@Beta
	public String setTagValue(final Tag tag, final String value) throws ExifError, JExifException {
		exifTool.writeTagInfo(file, tag, value);
		return getExactTagValue(tag);
	}

	/**
	 * Writes GPS info.
	 *
	 * @param gpsLatitude gps latitude
	 * @param gpsLatitudeRef N or S (Northern or Southern hemisphere)
	 * @param gpsLongitude gps longitude
	 * @param gpsLongitudeRef E or W (Eastern or Western)
	 * @param gpsAltitude gps altitude (always a positive value).
	 * @param gpsAltitudeRef 0 or 1 (above sea level or below sea level)
	 * @throws JExifException in case something goes wrong internally.
	 * @throws ExifError if something went wrong in ExifTool
	 */
	@Beta
	public void setGPSInfo(final String gpsLatitude, final String gpsLatitudeRef, final String gpsLongitude, final String gpsLongitudeRef, final String gpsAltitude, final String gpsAltitudeRef) throws ExifError, JExifException {
		Map<Tag, String> valuesToWrite = new HashMap<Tag, String>(6);
		valuesToWrite.put(ExifGPS.GPSALTITUDE, gpsAltitude);
		valuesToWrite.put(ExifGPS.GPSALTITUDEREF, gpsAltitudeRef);
		valuesToWrite.put(ExifGPS.GPSLATITUDE, gpsLatitude);
		valuesToWrite.put(ExifGPS.GPSLATITUDEREF, gpsLatitudeRef);
		valuesToWrite.put(ExifGPS.GPSLONGITUDE, gpsLongitude);
		valuesToWrite.put(ExifGPS.GPSLONGITUDEREF, gpsLongitudeRef);
		exifTool.writeGPSTagInfo(file, valuesToWrite);
	}

	/**
	 * Reads out all the tag values in human read-able format, even the ones not known in J-ExifTool.<br />
	 * The default set supported by J-ExifTool can be found in the be.pw.jexif.enums.tag package.<br />
	 * The set of known tags can be extended by using {@link be.pw.jexif.internal.util.TagUtil#register(Class)}.
	 *
	 * @return a map with all the tag values.
	 * @throws JExifException in case something goes wrong internally.
	 * @throws ExifError if something went wrong in ExifTool
	 */
	@Beta
	public Map<String, String> getAllTagsValues() throws ExifError, JExifException {
		return exifTool.getAllTagInfo(file, false);
	}

	/**
	 * Reads out all the tag values in exact format, even the ones not known in J-ExifTool.<br />
	 * The default set supported by J-ExifTool can be found in the be.pw.jexif.enums.tag package.<br />
	 * The set of known tags can be extended by using {@link be.pw.jexif.internal.util.TagUtil#register(Class)}.
	 *
	 * @return a map with all the tag values.
	 * @throws JExifException in case something goes wrong internally.
	 * @throws ExifError if something went wrong in ExifTool
	 */
	@Beta
	public Map<String, String> getAllExactTagsValues() throws ExifError, JExifException {
		return exifTool.getAllTagInfo(file, true);
	}

	/**
	 * Reads out all the tag values in human read-able format, limited to only the tags known to J-ExifTool.<br />
	 * The default set supported by J-ExifTool can be found in the be.pw.jexif.enums.tag package.<br />
	 * The set of known tags can be extended by using {@link be.pw.jexif.internal.util.TagUtil#register(Class)}.
	 *
	 * @return a map with all the tag values.
	 * @throws JExifException in case something goes wrong internally.
	 * @throws ExifError if something went wrong in ExifTool
	 * @see be.pw.jexif.enums.tag
	 */
	@Beta
	public Map<Tag, String> getAllSupportedTagsValues() throws ExifError, JExifException {
		return exifTool.getAllSupportedTagInfo(file, false);
	}

	/**
	 * Reads out all the tag values in exact format, limited to only the tags known to J-ExifTool. <br />
	 * The default set supported by J-ExifTool can be found in the be.pw.jexif.enums.tag package.<br />
	 * The set of known tags can be extended by using {@link be.pw.jexif.internal.util.TagUtil#register(Class)}.
	 *
	 * @return a map with all the tag values.
	 * @throws JExifException in case something goes wrong internally.
	 * @throws ExifError if something went wrong in ExifTool
	 * @see be.pw.jexif.enums.tag
	 */
	@Beta
	public Map<Tag, String> getAllSupportedExactTagsValues() throws ExifError, JExifException {
		return exifTool.getAllSupportedTagInfo(file, true);
	}

	/**
	 * Performs a time shift on a tag containing a date, time or date-time value.
	 * <p>
	 * Any date, time or datetime value can be shifted in any direction. The amount of shift is determined using the shiftPattern param. The shiftPattern must be any of those described on {@link http://www.sno.phy.queensu.ca/~phil/exiftool/Shift.html}.
	 * <p>
	 * A utility class which creates some straightforward patterns can be used to: {@link be.pw.jexif.internal.util.TimeShiftGenerator}
	 * <p>
	 * Optionally you can shift all datetime tags by passing the {@link DateTag#ALLDATES} shortcut
	 *
	 * @param dateTag the tag to modify
	 * @param shiftPattern the pattern (see doc)
	 * @return the new, timeshifted value
	 * @throws JExifException in case something goes wrong internally.
	 * @throws ExifError if something went wrong in ExifTool
	 * @since v0.0.6
	 */
	@Beta
	public String timeShift(final DateTag dateTag, final String shiftPattern) throws JExifException, ExifError {
		exifTool.timeShift(file, shiftPattern, dateTag);
		return getTagValue(dateTag.getTag());
	}

	/**
	 * Copies tags from one file to another.
	 *
	 * @param from the source file. The tags specified by the tags parameter will be read from this file
	 * @param tags the tag to read. If left empty, <b>all</b> tags will be copied !
	 * @throws JExifException in case something goes wrong internally.
	 * @throws ExifError if something went wrong in ExifTool
	 * @since v0.0.6
	 */
	@Beta
	public void copyFrom(final JExifInfo from, final Tag... tags) throws JExifException, ExifError {
		exifTool.copyFrom(file, from.getFile(), tags);
	}

	/**
	 * Removes all known exif tags from an image file.
	 *
	 * @throws JExifException if something goes wrong internally
	 * @throws ExifError if somethinig goes wrong in ExifTool
	 * @since v0.0.8
	 */
	@Beta
	public void deleteAllExifTags() throws JExifException, ExifError {
		exifTool.deleteAllExifTags(file);
	}

	/**
	 * Extract a thumbnail image (if present) to a file.
	 * <p>
	 * The format defines the thumbnail's filename and defaults to {orginalName}_tn.{originalExt} and will be created in the same folder as the file.
	 * <p>
	 * Extraction to a different folder can be done by passing an absolute or relative path with the '%f' placeholder which will be replaced by the original filename.
	 *
	 * @param format the thumbnail file name format
	 * @throws JExifException in case something goes wrong internally.
	 * @throws ExifError if something went wrong in ExifTool
	 * @since v0.0.8
	 */
	@Beta
	public void extractThumbnail(final String format) throws JExifException, ExifError {
		exifTool.extractThumbnail(file, format);
	}

	/**
	 * Returns a reference to the file.
	 *
	 * @return the file.
	 */
	File getFile() {
		return file;
	}
}
