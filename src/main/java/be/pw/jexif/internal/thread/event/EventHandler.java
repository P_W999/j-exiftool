/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.thread.event;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import com.google.common.eventbus.Subscribe;

import be.pw.jexif.internal.constants.ExecutionConstant;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * Handles the different events.
 *
 * @author phillip
 * @see be.pw.jexif.internal.thread.event.InputEvent
 * @see be.pw.jexif.internal.thread.event.ErrorEvent
 * @see com.google.common.eventbus.EventBus
 */
@Slf4j
@Getter
public class EventHandler {

	/**
	 * Indicates that the reader has finished it's reading. <br />
	 * The reader has finished as soon as {@link be.pw.jexif.internal.constants.ExecutionConstant#STOP} has been read.
	 */
	private boolean finished = false;

	/**
	 * Handles an input-message.
	 *
	 * @param input the input message containing a single ExifTool output line.
	 */
	@Subscribe
	public void handle(final InputEvent input) {
		handle(input.getMsg(), false);
	}

	/**
	 * Handles an error-message.
	 *
	 * @param error the error message containing a single ExifTool output line.
	 */
	@Subscribe
	public void handle(final ErrorEvent error) {
		handle(error.getMsg(), true);
	}

	private String currentUID = null;

	private final List<String> resultList = new ArrayList<>();

	private final List<String> errorList = new ArrayList<>();

	private synchronized void handle(final String line, final boolean error) {
		log.trace("EventLine={}", line);
		if (line.startsWith(ExecutionConstant.START)) {
			currentUID = line.substring(ExecutionConstant.START.length());
			log.trace("CurrentUID = {}", currentUID);
		} else if (line.startsWith(ExecutionConstant.STOP)) {
			if (!line.substring(ExecutionConstant.STOP.length()).equals(currentUID)) {
				throw new RuntimeException(MessageFormat.format("JExifOuput was desynchronized, expected {0} but got {1}", currentUID, line.substring(ExecutionConstant.STOP.length())));
			}
			finished = true;
		} else {
			if (error) {
				log.trace("Adding {} to error list", line);
				errorList.add(line);
			} else {
				log.trace("Adding {} to output list", line);
				resultList.add(line);
			}

		}
	}

}
