/*******************************************************************************
 * Copyright 2013 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.action.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

import be.pw.jexif.enums.DateTag;
import be.pw.jexif.enums.Errors;
import be.pw.jexif.exception.JExifException;
import be.pw.jexif.internal.action.IDateTimeShiftAction;
import be.pw.jexif.internal.constants.ExecutionConstant;
import be.pw.jexif.internal.util.ArrayUtil;
import be.pw.jexif.internal.util.Cal10nUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Action to perform date/time-shifts.
 * <p>
 * Usefull documentation can be found at {@link http://www.sno.phy.queensu.ca/~phil/exiftool/Shift.html}.
 *
 * @author phillip
 * @since v0.0.6
 *
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PACKAGE)	// use the factory to create a new instance
class DateTimeShiftAction extends AbstractAction implements IDateTimeShiftAction {

	/**
	 * The shift to perform without the shift direction.
	 */
	private String shift;

	/**
	 * The shift direction (either + or -).
	 */
	private String shiftDirection;

	/**
	 * The list of date tags which must be modified.
	 */
	private Collection<DateTag> datesToModify;

	/**
	 * Sets the params.
	 *
	 * @param file the file from which you would like to change the Exif tag values.
	 * @param shift a string containing the amount to be shifted. The format should be accepted by ExifTool {@link http://www.sno.phy.queensu.ca/~phil/exiftool/Shift.html}
	 * @param datesToModify a list with DateTags which should be time-shifted
	 * @throws IOException if the given file does not exist.
	 */
	public void setParams(final File file, final String shift, final Collection<DateTag> datesToModify) throws IOException {
		Preconditions.checkNotNull(file);
		if (!file.exists()) {
			throw new FileNotFoundException(Cal10nUtil.get(Errors.IO_FILE_NOT_VALID, file.getAbsolutePath()));
		}
		Preconditions.checkNotNull(shift);
		Preconditions.checkArgument(shift.matches("[\\d:\\+\\- ]*"), Cal10nUtil.get(Errors.VALIDATION_TIMESHIFT, shift));
		Preconditions.checkNotNull(datesToModify);
		Preconditions.checkArgument(datesToModify.size() != 0, Cal10nUtil.get(Errors.VALIDATION_TAGLIST));
		this.file = file;
		if (shift.startsWith("+")) {
			shiftDirection = "+";
			this.shift = shift.substring(1);
		} else if (shift.startsWith("-")) {
			shiftDirection = "-";
			this.shift = shift.substring(1);
		} else {
			shiftDirection = "+";
			this.shift = shift;
		}
		this.datesToModify = datesToModify;
		if (datesToModify.contains(DateTag.ALLDATES)) {
			datesToModify.removeAll(Lists.newArrayList(DateTag.CREATEDATE, DateTag.DATETIMEORIGINAL, DateTag.MODIFYDATE)); // ensure no doubles
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] buildArguments() throws JExifException {
		try {
			int size = datesToModify.size() + 2;
			String[] arguments = new String[size];
			int i = 0;
			for (DateTag tag : datesToModify) {
				arguments[i++] = "-" + tag.getTag().getName() + shiftDirection + "=" + shift;
			}
			arguments[i++] = ExecutionConstant.EXACT_VALUE;
			arguments[i] = file.getCanonicalPath();
			log.trace("Arguments are : " + ArrayUtil.toString(arguments));
			return arguments;
		} catch (IOException e) {
			throw new JExifException(Cal10nUtil.get(Errors.IO_BUILD_ARGUMENTS), e);
		}
	}

}
