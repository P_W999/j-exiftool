/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
/**
 *
 */
package be.pw.jexif.internal.action.impl;

import be.pw.jexif.exception.JExifException;
import be.pw.jexif.internal.action.ITagReadExactAction;
import be.pw.jexif.internal.constants.ExecutionConstant;
import be.pw.jexif.internal.util.ArrayUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * The TagReadExactAction will read out a Tag with it's exact value. So instead of an aperture of 2.8 you might get 2.79999456.
 *
 * @author phillip
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PACKAGE)	// use the factory to create a new instance
class TagReadExactAction extends TagReadAction implements ITagReadExactAction {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] buildArguments() throws JExifException {
		log.trace("Calling super.buildArguments()");
		String[] arguments = super.buildArguments();
		String[] newArguments = new String[arguments.length + 1];
		System.arraycopy(arguments, 0, newArguments, 0, arguments.length);
		newArguments[newArguments.length - 1] = ExecutionConstant.EXACT_VALUE; // third last argument should be -n
		log.trace("Arguments are actually : " + ArrayUtil.toString(newArguments));
		return newArguments;
	}
}
