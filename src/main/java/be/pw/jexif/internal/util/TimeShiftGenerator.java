/*******************************************************************************
 * Copyright 2013 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.util;

import lombok.experimental.UtilityClass;

/**
 * A utility class to create a timeshift string.
 * <p>
 * A timeshift can have many format as described here: {@link http://www.sno.phy.queensu.ca/~phil/exiftool/Shift.html}
 * <p>
 * The TimeShiftGenerator will only generated two simple and straightforward shifts:
 * <ul>
 * <li>[+/-]h:m:s or [+/-]Y:M:D
 * <li>Y:M:D h:m:s:[+/-]h
 * </ul>
 *
 * @author phillip
 * @since v0.0.6
 *
 */
@UtilityClass
public class TimeShiftGenerator {

	/**
	 * Separator between hour, min, sec or year, month, day.
	 */
	private static final String SEPARATOR = ":";

	/**
	 * Generates a timeshift String in the format Y:M:D or h:m:s
	 * <p>
	 * The positive parameter will defined whether it's a forward or backward shift. All passed arguments will be interpreted as positive numbers.
	 *
	 * @param positive whether the timeshift is towards the future (true) or to the past (false)
	 * @param largest either hour or year
	 * @param inbetween either minute or month
	 * @param smallest either second or day
	 * @return the formatted string
	 */
	public static String generateTimeShift(final boolean positive, final int largest, final int inbetween, final int smallest) {
		StringBuilder b = new StringBuilder();
		if (positive) {
			b.append("+");
		} else {
			b.append("-");
		}
		b.append(base(largest)).append(SEPARATOR);
		b.append(base(inbetween)).append(SEPARATOR);
		b.append(base(smallest));
		return b.toString();
	}

	/**
	 * Generates a timeshift String in the format Y:M:D h:m:s:[+/-]h
	 * All passed arguments will be interpreted as positive numbers, except for the timezone.
	 *
	 * @param positive whether the timeshift is towards the future (true) or to the past (false)
	 * @param year year
	 * @param month month
	 * @param day day
	 * @param hour hour
	 * @param minute minute
	 * @param second second
	 * @param timezone timezone hour shift (can be positive or negative)
	 * @return the formatted string
	 */
	public static String generateTimeShift(final boolean positive, final int year, final int month, final int day, final int hour, final int minute, final int second, final int timezone) {
		StringBuilder b = new StringBuilder();
		b.append(generateTimeShift(positive, year, month, day));
		b.append(" ");
		b.append(generateTimeShift(positive, hour, minute, second).substring(1));
		b.append(SEPARATOR);
		if (timezone != 0) {
			if (timezone > 0) {
				b.append("+").append(timezone);
			} else {
				b.append(timezone);
			}
		}
		return b.toString();
	}

	static int base(final int i) {
		if (i < 0) {
			return -1 * i;
		} else {
			return i;
		}
	}
}
