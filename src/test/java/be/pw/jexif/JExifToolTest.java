/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif;

import static org.fest.assertions.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

import org.fest.assertions.Fail;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import be.pw.jexif.enums.DataType;
import be.pw.jexif.enums.DateTag;
import be.pw.jexif.enums.tag.ExifIFD;
import be.pw.jexif.enums.tag.IFD0;
import be.pw.jexif.enums.tag.Tag;
import be.pw.jexif.exception.ExifError;
import be.pw.jexif.exception.JExifException;
import be.pw.jexif.internal.constants.ExecutionConstant;
import be.pw.jexif.internal.util.DateUtil;
import be.pw.jexif.internal.util.ExiftoolPathUtil;
import be.pw.jexif.internal.util.TagUtil;

import com.google.common.io.Files;

@Listeners({ JExifToolTestInvokeMethodListener.class })
public class JExifToolTest {

	private JExifTool tool;
	private final File read01 = new File(JExifToolTest.class.getResource("/read01.JPG").getFile());
	private final File read02 = new File(JExifToolTest.class.getResource("/read02.JPG").getFile());
	private final File read03 = new File(JExifToolTest.class.getResource("/read03.JPG").getFile());
	private final File read04 = new File(JExifToolTest.class.getResource("/read04.JPG").getFile());
	private final File read05 = new File(JExifToolTest.class.getResource("/read05.JPG").getFile());
	private final File read06 = new File(JExifToolTest.class.getResource("/read06.JPG").getFile());
	private final File readUc = new File(JExifToolTest.class.getResource("/read_unicode.JPG").getFile());
	private final String unicodeSoftware = "會意字"; // this might be screwed up in windows, even if workspace is configured for UTF-8
	private final String frenchMake = "ééé";
	private File write01 = null;
	private File write02 = null;
	private final File exifTool; 

	public JExifToolTest() {
		File et = ExiftoolPathUtil.getPath();
		if (!et.exists()) {
			if (System.getProperty("os.name").toLowerCase().contains("windows")) {
				et = new File("exiftool/exiftool.exe");
			} else {
				et = new File("exiftool/exiftool");
			}
		}
		
		assertThat(et).as("The exifTool executable is missing. Either set it correctly via system property, environment variable or place it in the exiftool folder at the root of this project.").exists();
		exifTool = et;
	}
	
	private File thumbnail = null;

	@BeforeClass
	public void beforeClass() {
		assertThat(read01).as("A test JPG file is missing from the test-resources folder").exists();
		assertThat(read02).as("A test JPG file is missing from the test-resources folder").exists();
		assertThat(read03).as("A test JPG file is missing from the test-resources folder").exists();
		assertThat(read04).as("A test JPG file is missing from the test-resources folder").exists();
		assertThat(read05).as("A test JPG file is missing from the test-resources folder").exists();
		assertThat(read06).as("A test JPG file is missing from the test-resources folder").exists();
		assertThat(readUc).as("A test JPG file is missing from the test-resources folder").exists();
		
		// just to make sure we don't write anything to these files.
		read01.setReadOnly();
		read02.setReadOnly();
		read03.setReadOnly();
		read04.setReadOnly();
		read05.setReadOnly();
		read06.setReadOnly();
		readUc.setReadOnly();
	}

	@BeforeMethod(alwaysRun = true)
	public void beforeTest() {
		System.setProperty(ExecutionConstant.EXIFTOOLPATH, exifTool.getAbsolutePath());
		// System.setProperty(ExecutionConstant.EXIFTOOLDEADLOCK, Integer.toString(Integer.MAX_VALUE));
		try {
			tool = new JExifTool();
		} catch (JExifException e) {
			Fail.fail(e.getMessage());
		}
	}

	@BeforeMethod(groups = { "write", "medium" }, alwaysRun = true)
	public void beforeWrite() throws Exception {
		write01 = File.createTempFile("testng_tmp_01", ".jpg");
		write02 = File.createTempFile("testng_tmp_00", ".jpg");
		Files.copy(read01, write01);
		Files.copy(read02, write02);
		assertThat(write01).as("A temporary file is missing").exists();
		assertThat(write02).as("A temporary file is missing").exists();
	}

	@AfterMethod(alwaysRun = true)
	public void afterTest() throws InterruptedException, JExifException {
		tool.stop();
	}

	@AfterMethod(groups = { "write" })
	public void afterWrite() {
		write01.delete();
		write02.delete();
	}

	@AfterMethod(groups = { "thumbnail" })
	public void deleteThumbnails() {
		if (thumbnail != null) {
			if (thumbnail.exists()) {
				thumbnail.delete();
			}
		}
	}

	/*
	 * The following tests are simple test. They test the basic functionalities like reading or writing one or more Tags. These test are simple because they don't get into details like error handling, reading and writing a huge Tag set or copying Tags.
	 */

	@Test(groups = { "read-human", "simple", "string" }, suiteName = "simple")
	public void testReadSingleFileSingleTagAsStringInHumanReadableFormat() throws Exception {
		JExifInfo info = tool.getInfo(read01);
		assertThat(info.getTagValue(IFD0.MAKE)).isEqualTo("CASIO COMPUTER CO.,LTD.");
	}

	@Test(enabled = false, groups = { "read-human", "medium", "string" }, suiteName = "medium", dependsOnMethods = { "testReadSingleFileSingleFrenchTagAsStringInHumanReadableFormat" })
	public void testReadSingleFileSingleAsianTagAsStringInHumanReadableFormat() throws Exception {
		JExifInfo info = tool.getInfo(readUc);
		assertThat(info.getTagValue(IFD0.SOFTWARE)).isEqualTo(unicodeSoftware);
	}

	@Test(enabled = false, groups = { "read-human", "medium", "string" }, suiteName = "medium")
	public void testReadSingleFileSingleFrenchTagAsStringInHumanReadableFormat() throws Exception {
		JExifInfo info = tool.getInfo(readUc);
		assertThat(info.getTagValue(IFD0.MAKE)).isEqualTo(frenchMake);
	}

	@Test(groups = { "read-exact", "simple", "string" }, suiteName = "simple")
	public void testReadSingleFileSingleTagAsStringInExactFormat() throws Exception {
		JExifInfo info = tool.getInfo(read01);
		assertThat(info.getExactTagValue(IFD0.MAKE)).isEqualTo("CASIO COMPUTER CO.,LTD.");
	}

	@Test(groups = { "read-human", "simple", "digit" }, suiteName = "simple")
	public void testReadSingleFileSingleTagAsDigitInHumanReadableFormat() throws Exception {
		JExifInfo info = tool.getInfo(read02);
		assertThat(info.getTagValue(ExifIFD.EXPOSURETIME)).isEqualTo("1/400");
	}

	@Test(groups = { "read-exact", "simple", "digit" }, suiteName = "simple")
	public void testReadSingleFileSingleTagAsDigitInExactFormat() throws Exception {
		JExifInfo info = tool.getInfo(read02);
		assertThat(info.getExactTagValue(ExifIFD.EXPOSURETIME)).isEqualTo("0.0025");
	}

	@Test(groups = { "read-human", "simple", "date" }, suiteName = "simple")
	public void testReadSingleFileSingleTagAsDateInHumanReadableFormat() throws Exception {
		JExifInfo info = tool.getInfo(read01);
		assertThat(info.getTagValue(ExifIFD.DATETIMEORIGINAL)).isEqualTo("2010:08:12 21:19:01");
	}

	@Test(groups = { "read-exact", "simple", "date" }, suiteName = "simple")
	public void testReadSingleFileSingleTagAsDateInExactFormat() throws Exception {
		JExifInfo info = tool.getInfo(read01);
		assertThat(info.getExactTagValue(ExifIFD.DATETIMEORIGINAL)).isEqualTo("2010:08:12 21:19:01");
	}

	@Test(groups = { "read-human", "simple" }, suiteName = "simple")
	public void testReadSingleFileMultipleTagsInHumanReadableFormat() throws Exception {
		JExifInfo info = tool.getInfo(read03);
		String apertureValue = info.getTagValue(ExifIFD.APERTUREVALUE);
		String focalLength = info.getTagValue(ExifIFD.FOCALLENGTH);
		String fNumber = info.getTagValue(ExifIFD.FNUMBER);
		String iso = info.getTagValue(ExifIFD.ISO);
		assertThat(apertureValue).isEqualTo("4.8");
		assertThat(focalLength).isEqualTo("8.7 mm");
		assertThat(fNumber).isEqualTo("4.7");
		assertThat(iso).isEqualTo("1600");
	}

	@Test(groups = { "read-exact", "simple" }, suiteName = "simple")
	public void testReadSingleFileMultipleTagsInExactFormat() throws Exception {
		JExifInfo info = tool.getInfo(read03);
		String apertureValue = info.getExactTagValue(ExifIFD.APERTUREVALUE);
		String focalLength = info.getExactTagValue(ExifIFD.FOCALLENGTH);
		String fNumber = info.getExactTagValue(ExifIFD.FNUMBER);
		String iso = info.getExactTagValue(ExifIFD.ISO);
		assertThat(apertureValue).isEqualTo("4.75682846001088");
		assertThat(focalLength).isEqualTo("8.7");
		assertThat(fNumber).isEqualTo("4.7");
		assertThat(iso).isEqualTo("1600");
	}

	@Test(groups = { "read-human", "simple", "error" }, suiteName = "simple", expectedExceptions = { IOException.class }, expectedExceptionsMessageRegExp = "There was a problem reading the file .*fail.jpg")
	public void testExifErrorOnFileNotFoundHumanReadable() throws Exception {
		File nonExistingFile = new File("c:\\bla\\haha\\lol\\fail.jpg");
		JExifInfo info = tool.getInfo(nonExistingFile);
		info.getTagValue(ExifIFD.APERTUREVALUE);
	}

	@Test(groups = { "read-exact", "simple", "error" }, suiteName = "simple", expectedExceptions = { IOException.class }, expectedExceptionsMessageRegExp = "There was a problem reading the file .*fail.jpg")
	public void testExifErrorOnFileNotFoundExact() throws Exception {
		File nonExistingFile = new File("c:\\bla\\haha\\lol\\fail.jpg");
		JExifInfo info = tool.getInfo(nonExistingFile);
		info.getExactTagValue(ExifIFD.APERTUREVALUE);
	}

	@Test(enabled = false, groups = { "read-exact", "simple", "error" }, suiteName = "simple", expectedExceptions = { JExifException.class }, expectedExceptionsMessageRegExp = ".*Deadlocked.*")
	public void testDeadLock() throws Exception, ExifError {
		JExifInfo deadlock = tool.getInfo(read01);
		tool.stop();
		deadlock.getTagValue(ExifIFD.ISO);
	}

	@Test(groups = { "simple", "write", "string" }, suiteName = "simple")
	public void testWriteStringTag() throws Exception {
		JExifInfo info = tool.getInfo(write01);
		assertThat(info.getTagValue(IFD0.MAKE)).as("Failed to correctly read test file").isEqualTo("CASIO COMPUTER CO.,LTD.");
		String result = info.setTagValue(IFD0.MAKE, "P_W999");
		assertThat(result).as("Failed to write the tag").isEqualTo("P_W999");
		assertThat(info.getTagValue(IFD0.MAKE)).isEqualTo("P_W999");
	}

	@Test(groups = { "simple", "write", "digit" }, suiteName = "simple")
	public void testWriteDoubleTagAsInt() throws Exception {
		JExifInfo info = tool.getInfo(write02);
		assertThat(info.getTagValue(ExifIFD.EXPOSURETIME)).as("Failed to correctly read test file").isEqualTo("1/400");
		String result = info.setTagValue(ExifIFD.EXPOSURETIME, "1");
		assertThat(result).as("Failed to write the tag").isEqualTo("1");
		assertThat(info.getTagValue(ExifIFD.EXPOSURETIME)).isEqualTo("1");
	}

	@Test(groups = { "simple", "write", "digit" }, suiteName = "simple")
	public void testWriteDoubleTagAsDouble() throws Exception {
		JExifInfo info = tool.getInfo(write02);
		assertThat(info.getTagValue(ExifIFD.EXPOSURETIME)).as("Failed to correctly read test file").isEqualTo("1/400");
		String result = info.setTagValue(ExifIFD.EXPOSURETIME, "0.05");
		assertThat(result).as("Failed to write the tag").isEqualTo("0.05");
		assertThat(info.getTagValue(ExifIFD.EXPOSURETIME)).isEqualTo("1/20");
	}

	@Test(groups = { "simple", "write", "error" }, suiteName = "simple", expectedExceptions = { JExifException.class }, expectedExceptionsMessageRegExp = "Tag ExposureTime has datatype RAT64U. The value P_W999 could not be parsed as being such.")
	public void testWriteDoubleTagAsString() throws Exception {
		JExifInfo info = tool.getInfo(write02);
		assertThat(info.getTagValue(ExifIFD.EXPOSURETIME)).as("Failed to correctly read test file").isEqualTo("1/400");
		info.setTagValue(ExifIFD.EXPOSURETIME, "P_W999");
	}

	@Test(enabled = false, groups = { "simple", "write", "date" }, suiteName = "simple")
	public void testWriteDateAsDate() throws Exception {
		String date = DateUtil.fromDate(new Date());
		JExifInfo info = tool.getInfo(write01);
		assertThat(info.getTagValue(ExifIFD.DATETIMEORIGINAL)).as("Failed to correctly read test file").isEqualTo("2010:08:12 21:19:01");
		String result = info.setTagValue(ExifIFD.DATETIMEORIGINAL, date);
		assertThat(result).isEqualTo(date);
		assertThat(info.getTagValue(ExifIFD.DATETIMEORIGINAL)).isEqualTo(date);
	}

	@Test(enabled = false, groups = { "simple", "write", "error" }, suiteName = "simple", expectedExceptions = { JExifException.class }, expectedExceptionsMessageRegExp = "An error occured while parsing the value P_W999 for Tag DATE_TIME_ORIGINAL which should be of type Date")
	public void testWriteDateAsString() throws Exception {
		JExifInfo info = tool.getInfo(write01);
		assertThat(info.getTagValue(ExifIFD.DATETIMEORIGINAL)).as("Failed to correctly read test file").isEqualTo("2010:08:12 21:19:01");
		info.setTagValue(ExifIFD.DATETIMEORIGINAL, "P_W999");
	}

	/*
	 * These are medium complexity tests. A single test will use invoke multiple (different) actions. The idea is that J-ExifTool is capable of handling many different things without restarting. The groups "error", "digit", "string" and "date" won't be used anymore since a single test should cover it all. Also, I'm aware that these are integration tests and not unit-tests.
	 */

	@Test(groups = { "medium", "write", "read-exact", "read-human" })
	public void testSetOne() throws Exception {
		JExifInfo info1 = tool.getInfo(read01);
		JExifInfo info2 = tool.getInfo(read02);
		JExifInfo info3 = tool.getInfo(write01);
		JExifInfo info4 = tool.getInfo(write02);

		assertThat(info1.getTagValue(IFD0.MODEL)).isEqualTo("EX-FH100");
		assertThat(info2.getTagValue(IFD0.MODEL)).isEqualTo("NEX-7");
		assertThat(info3.setTagValue(IFD0.MODEL, info2.getTagValue(IFD0.MODEL))).isEqualTo("NEX-7"); // juk ! copyTag() method is coming soon ;)
		assertThat(info3.getExactTagValue(ExifIFD.EXPOSUREPROGRAM)).isEqualTo("2");
		assertThat(info4.getExactTagValue(ExifIFD.EXPOSUREPROGRAM)).isEqualTo("0");
		assertThat(info1.getTagValue(ExifIFD.EXPOSUREPROGRAM)).isEqualTo("Program AE");
		assertThat(info2.getTagValue(ExifIFD.EXPOSUREPROGRAM)).isEqualTo("Program AE");
		tool.stop();
		assertThat(info3.setTagValue(ExifIFD.SHARPNESS, info2.getExactTagValue(ExifIFD.SHARPNESS))).isEqualTo("0");
		assertThat(info4.setTagValue(ExifIFD.SHARPNESS, "1")).isEqualTo("1");
		tool.stop();
		try {
			assertThat(info4.setTagValue(ExifIFD.FNUMBER, "ERROR"));
			Fail.fail("Writing a string to a double-valued tag should throw an error");
		} catch (Exception e) {
			assertThat(e).isInstanceOf(JExifException.class);
		}
		assertThat(info1.getTagValue(ExifIFD.USERCOMMENT)).isNull(); // info 1
		assertThat(info2.getTagValue(ExifIFD.USERCOMMENT)).isNull(); // info 2
		assertThat(info3.setTagValue(ExifIFD.USERCOMMENT, "P_W999 writes exif tags")).isEqualTo("P_W999 writes exif tags");
		assertThat(info4.setTagValue(ExifIFD.USERCOMMENT, "P_W999 writes exif tags")).isEqualTo("P_W999 writes exif tags");
		assertThat(info1.getTagValue(ExifIFD.CONTRAST)).isEqualTo("Normal");
		assertThat(info2.getTagValue(ExifIFD.CONTRAST)).isEqualTo("Normal");
		assertThat(info3.setTagValue(ExifIFD.CONTRAST, "1")).isEqualTo("1");
		assertThat(info4.setTagValue(ExifIFD.CONTRAST, "2")).isEqualTo("2");
		assertThat(info3.getTagValue(ExifIFD.CONTRAST)).isEqualTo("Low");
		assertThat(info4.getTagValue(ExifIFD.CONTRAST)).isEqualTo("High");
		assertThat(info1.getTagValue(IFD0.ORIENTATION)).isEqualTo("Horizontal (normal)");
		assertThat(info2.getTagValue(IFD0.ORIENTATION)).isEqualTo("Horizontal (normal)");
		assertThat(info3.setTagValue(IFD0.ORIENTATION, "1")).isEqualTo("1");
		assertThat(info4.setTagValue(IFD0.ORIENTATION, "6")).isEqualTo("6");
		assertThat(info3.getTagValue(IFD0.ORIENTATION)).isEqualTo("Horizontal (normal)");
		assertThat(info4.getTagValue(IFD0.ORIENTATION)).isEqualTo("Rotate 90 CW");
		assertThat(info1.getTagValue(IFD0.XRESOLUTION)).isEqualTo("72");
		assertThat(info2.getTagValue(IFD0.XRESOLUTION)).isEqualTo("350");
		assertThat(info1.getTagValue(IFD0.XRESOLUTION)).isEqualTo("72");
		assertThat(info2.getTagValue(IFD0.XRESOLUTION)).isEqualTo("350");
		assertThat(info1.getTagValue(ExifIFD.DATETIMEORIGINAL)).isEqualTo("2010:08:12 21:19:01");
		assertThat(info2.getTagValue(ExifIFD.DATETIMEORIGINAL)).isEqualTo("2011:11:07 16:16:50");

	}

	@Test(groups = { "medium", "read-human" })
	public void testReadAllTags() throws Exception {
		JExifInfo i = tool.getInfo(read02);
		Map<String, String> r = i.getAllTagsValues();

		assertThat(r.get("Make")).isNotNull().isNotEmpty().isEqualTo("SONY");
		assertThat(r.get("Model")).isNotNull().isNotEmpty().isEqualTo("NEX-7");
		assertThat(r.get("Orientation")).isNotNull().isNotEmpty().isEqualTo("Horizontal (normal)");
		assertThat(r.get("XResolution")).isNotNull().isNotEmpty().isEqualTo("350");
		assertThat(r.get("YResolution")).isNotNull().isNotEmpty().isEqualTo("350");
		assertThat(r.get("ResolutionUnit")).isNotNull().isNotEmpty().isEqualTo("inches");
		assertThat(r.get("Software")).isNotNull().isNotEmpty().isEqualTo("NEX-7 v1.00");
		assertThat(r.get("ModifyDate")).isNotNull().isNotEmpty().isEqualTo("2011:11:07 16:16:50");
		assertThat(r.get("YCbCrPositioning")).isNotNull().isNotEmpty().isEqualTo("Co-sited");
		assertThat(r.get("ExposureTime")).isNotNull().isNotEmpty().isEqualTo("1/400");
		assertThat(r.get("FNumber")).isNotNull().isNotEmpty().isEqualTo("8.0");
		assertThat(r.get("ExposureProgram")).isNotNull().isNotEmpty().isEqualTo("Program AE");
		assertThat(r.get("ISO")).isNotNull().isNotEmpty().isEqualTo("100");
		assertThat(r.get("SensitivityType")).isNotNull().isNotEmpty().isEqualTo("Recommended Exposure Index");
		assertThat(r.get("RecommendedExposureIndex")).isNotNull().isNotEmpty().isEqualTo("100");
		assertThat(r.get("ExifVersion")).isNotNull().isNotEmpty().isEqualTo("0230");
		assertThat(r.get("DateTimeOriginal")).isNotNull().isNotEmpty().isEqualTo("2011:11:07 16:16:50");
		assertThat(r.get("CreateDate")).isNotNull().isNotEmpty().isEqualTo("2011:11:07 16:16:50");
		assertThat(r.get("ComponentsConfiguration")).isNotNull().isNotEmpty().isEqualTo("Y, Cb, Cr, -");
		assertThat(r.get("CompressedBitsPerPixel")).isNotNull().isNotEmpty().isEqualTo("3");
		assertThat(r.get("BrightnessValue")).isNotNull().isNotEmpty().isEqualTo("9.05625");
		assertThat(r.get("ExposureCompensation")).isNotNull().isNotEmpty().isEqualTo("-0.3");
		assertThat(r.get("MaxApertureValue")).isNotNull().isNotEmpty().isEqualTo("4.0");
		assertThat(r.get("MeteringMode")).isNotNull().isNotEmpty().isEqualTo("Multi-segment");
		assertThat(r.get("LightSource")).isNotNull().isNotEmpty().isEqualTo("Unknown");
		assertThat(r.get("Flash")).isNotNull().isNotEmpty().isEqualTo("Off, Did not fire");
		assertThat(r.get("FocalLength")).isNotNull().isNotEmpty().isEqualTo("28.0 mm");
		assertThat(r.get("PreviewImage")).isNotNull().isNotEmpty().isEqualTo("(Binary data 973248 bytes, use -b option to extract)");
		assertThat(r.get("Rating")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("Brightness")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("LongExposureNoiseReduction")).isNotNull().isNotEmpty().isEqualTo("Off");
		assertThat(r.get("HighISONoiseReduction")).isNotNull().isNotEmpty().isEqualTo("Normal");
		assertThat(r.get("HDR")).isNotNull().isNotEmpty().isEqualTo("Off; Uncorrected image");
		assertThat(r.get("WBShiftAB_GM")).isNotNull().isNotEmpty().isEqualTo("0 0");
		assertThat(r.get("FaceInfoOffset")).isNotNull().isNotEmpty().isEqualTo("94");
		assertThat(r.get("SonyDateTime")).isNotNull().isNotEmpty().isEqualTo("2011:11:07 16:16:50");
		assertThat(r.get("SonyImageWidth")).isNotNull().isNotEmpty().isEqualTo("6000");
		assertThat(r.get("FacesDetected")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("FaceInfoLength")).isNotNull().isNotEmpty().isEqualTo("37");
		assertThat(r.get("MetaVersion")).isNotNull().isNotEmpty().isEqualTo("DC7303320222000");
		assertThat(r.get("CreativeStyle")).isNotNull().isNotEmpty().isEqualTo("Standard");
		assertThat(r.get("ColorTemperature")).isNotNull().isNotEmpty().isEqualTo("Auto");
		assertThat(r.get("ColorCompensationFilter")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("SceneMode")).isNotNull().isNotEmpty().isEqualTo("Standard");
		assertThat(r.get("ZoneMatching")).isNotNull().isNotEmpty().isEqualTo("ISO Setting Used");
		assertThat(r.get("DynamicRangeOptimizer")).isNotNull().isNotEmpty().isEqualTo("Auto");
		assertThat(r.get("ImageStabilization")).isNotNull().isNotEmpty().isEqualTo("On");
		assertThat(r.get("ColorMode")).isNotNull().isNotEmpty().isEqualTo("Standard");
		assertThat(r.get("FullImageSize")).isNotNull().isNotEmpty().isEqualTo("6000x4000");
		assertThat(r.get("PreviewImageSize")).isNotNull().isNotEmpty().isEqualTo("1616x1080");
		assertThat(r.get("FileFormat")).isNotNull().isNotEmpty().isEqualTo("ARW 2.3");
		assertThat(r.get("Quality")).isNotNull().isNotEmpty().isEqualTo("Fine");
		assertThat(r.get("FlashExposureComp")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("WhiteBalanceFineTune")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("SonyModelID")).isNotNull().isNotEmpty().isEqualTo("NEX-7");
		assertThat(r.get("Teleconverter")).isNotNull().isNotEmpty().isEqualTo("None");
		assertThat(r.get("MultiFrameNoiseReduction")).isNotNull().isNotEmpty().isEqualTo("Off");
		assertThat(r.get("PictureEffect")).isNotNull().isNotEmpty().isEqualTo("Off");
		assertThat(r.get("SoftSkinEffect")).isNotNull().isNotEmpty().isEqualTo("Off");
		assertThat(r.get("VignettingCorrection")).isNotNull().isNotEmpty().isEqualTo("Auto");
		assertThat(r.get("LateralChromaticAberration")).isNotNull().isNotEmpty().isEqualTo("Auto");
		assertThat(r.get("DistortionCorrectionSetting")).isNotNull().isNotEmpty().isEqualTo("Off");
		assertThat(r.get("LensType")).isNotNull().isNotEmpty().isEqualTo("E-Mount, T-Mount, Other Lens or no lens");
		assertThat(r.get("LensSpec")).isNotNull().isNotEmpty().isEqualTo("E 18-55mm F3.5-5.6 OSS");
		assertThat(r.get("FlashLevel")).isNotNull().isNotEmpty().isEqualTo("Normal");
		assertThat(r.get("ReleaseMode")).isNotNull().isNotEmpty().isEqualTo("Exposure Bracketing");
		assertThat(r.get("SequenceNumber")).isNotNull().isNotEmpty().isEqualTo("2");
		assertThat(r.get("Anti-Blur")).isNotNull().isNotEmpty().isEqualTo("On (Shooting)");
		assertThat(r.get("IntelligentAuto")).isNotNull().isNotEmpty().isEqualTo("Off");
		assertThat(r.get("WhiteBalance")).isNotNull().isNotEmpty().isEqualTo("Auto");
		assertThat(r.get("SequenceImageNumber")).isNotNull().isNotEmpty().isEqualTo("2");
		assertThat(r.get("SequenceFileNumber")).isNotNull().isNotEmpty().isEqualTo("2");
		assertThat(r.get("ReleaseMode2")).isNotNull().isNotEmpty().isEqualTo("Continuous - Exposure Bracketing");
		assertThat(r.get("ShotNumberSincePowerUp")).isNotNull().isNotEmpty().isEqualTo("2");
		assertThat(r.get("SequenceLength")).isNotNull().isNotEmpty().isEqualTo("3 shots");
		assertThat(r.get("CameraOrientation")).isNotNull().isNotEmpty().isEqualTo("Horizontal (normal)");
		assertThat(r.get("Quality2")).isNotNull().isNotEmpty().isEqualTo("JPEG");
		assertThat(r.get("SonyImageHeight")).isNotNull().isNotEmpty().isEqualTo("4000");
		assertThat(r.get("ModelReleaseYear")).isNotNull().isNotEmpty().isEqualTo("2011");
		assertThat(r.get("AmbientTemperature")).isNotNull().isNotEmpty().isEqualTo("15 C");
		assertThat(r.get("FocusMode")).isNotNull().isNotEmpty().isEqualTo("AF-S");
		assertThat(r.get("AFAreaMode")).isNotNull().isNotEmpty().isEqualTo("Multi");
		assertThat(r.get("FocusPosition2")).isNotNull().isNotEmpty().isEqualTo("255");
		assertThat(r.get("LensZoomPosition")).isNotNull().isNotEmpty().isEqualTo("29%");
		assertThat(r.get("DistortionCorrection")).isNotNull().isNotEmpty().isEqualTo("None");
		assertThat(r.get("BatteryTemperature")).isNotNull().isNotEmpty().isEqualTo("25.0 C");
		assertThat(r.get("BatteryLevel")).isNotNull().isNotEmpty().isEqualTo("108%");
		assertThat(r.get("ReleaseMode3")).isNotNull().isNotEmpty().isEqualTo("Bracketing");
		assertThat(r.get("StopsAboveBaseISO")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("HDRSetting")).isNotNull().isNotEmpty().isEqualTo("Off");
		assertThat(r.get("PictureEffect2")).isNotNull().isNotEmpty().isEqualTo("Off");
		assertThat(r.get("WB_RGBLevels")).isNotNull().isNotEmpty().isEqualTo("635 256 373");
		assertThat(r.get("SonyISO")).isNotNull().isNotEmpty().isEqualTo("100");
		assertThat(r.get("FlashStatus")).isNotNull().isNotEmpty().isEqualTo("Built-in Flash present");
		assertThat(r.get("ImageCount")).isNull();
		assertThat(r.get("SonyExposureTime")).isNotNull().isNotEmpty().isEqualTo("1/379");
		assertThat(r.get("SonyFNumber")).isNotNull().isNotEmpty().isEqualTo("8.1");
		assertThat(r.get("LensMount")).isNotNull().isNotEmpty().isEqualTo("E-mount");
		assertThat(r.get("LensFormat")).isNotNull().isNotEmpty().isEqualTo("APS-C");
		assertThat(r.get("LensType2")).isNotNull().isNotEmpty().isEqualTo("Sony E 18-55mm F3.5-5.6 OSS");
		assertThat(r.get("LensSpecFeatures")).isNotNull().isNotEmpty().isEqualTo("E OSS");
		assertThat(r.get("ImageCount3")).isNull();
		assertThat(r.get("FlashpixVersion")).isNotNull().isNotEmpty().isEqualTo("0100");
		assertThat(r.get("ColorSpace")).isNotNull().isNotEmpty().isEqualTo("sRGB");
		assertThat(r.get("ExifImageWidth")).isNotNull().isNotEmpty().isEqualTo("1024");
		assertThat(r.get("ExifImageHeight")).isNotNull().isNotEmpty().isEqualTo("683");
		assertThat(r.get("InteropIndex")).isNotNull().isNotEmpty().isEqualTo("R98 - DCF basic file (sRGB)");
		assertThat(r.get("InteropVersion")).isNotNull().isNotEmpty().isEqualTo("0100");
		assertThat(r.get("FileSource")).isNotNull().isNotEmpty().isEqualTo("Digital Camera");
		assertThat(r.get("SceneType")).isNotNull().isNotEmpty().isEqualTo("Directly photographed");
		assertThat(r.get("CustomRendered")).isNotNull().isNotEmpty().isEqualTo("Normal");
		assertThat(r.get("ExposureMode")).isNotNull().isNotEmpty().isEqualTo("Auto bracket");
		assertThat(r.get("FocalLengthIn35mmFormat")).isNotNull().isNotEmpty().isEqualTo("42 mm");
		assertThat(r.get("SceneCaptureType")).isNotNull().isNotEmpty().isEqualTo("Standard");
		assertThat(r.get("Contrast")).isNotNull().isNotEmpty().isEqualTo("Normal");
		assertThat(r.get("Saturation")).isNotNull().isNotEmpty().isEqualTo("Normal");
		assertThat(r.get("Sharpness")).isNotNull().isNotEmpty().isEqualTo("Normal");
		assertThat(r.get("LensInfo")).isNotNull().isNotEmpty().isEqualTo("18-55mm f/3.5-5.6");
		assertThat(r.get("LensModel")).isNotNull().isNotEmpty().isEqualTo("E 18-55mm F3.5-5.6 OSS");
		assertThat(r.get("PrintIMVersion")).isNotNull().isNotEmpty().isEqualTo("0300");
		assertThat(r.get("Compression")).isNotNull().isNotEmpty().isEqualTo("JPEG (old-style)");
		assertThat(r.get("ThumbnailOffset")).isNotNull().isNotEmpty().isEqualTo("40290");
		assertThat(r.get("ThumbnailLength")).isNotNull().isNotEmpty().isEqualTo("11489");
		assertThat(r.get("ImageWidth")).isNotNull().isNotEmpty().isEqualTo("1024");
		assertThat(r.get("ImageHeight")).isNotNull().isNotEmpty().isEqualTo("683");
		assertThat(r.get("EncodingProcess")).isNotNull().isNotEmpty().isEqualTo("Baseline DCT, Huffman coding");
		assertThat(r.get("BitsPerSample")).isNotNull().isNotEmpty().isEqualTo("8");
		assertThat(r.get("ColorComponents")).isNotNull().isNotEmpty().isEqualTo("3");
		assertThat(r.get("YCbCrSubSampling")).isNotNull().isNotEmpty().isEqualTo("YCbCr4:4:4 (1 1)");
		assertThat(r.get("Aperture")).isNotNull().isNotEmpty().isEqualTo("8.0");
		assertThat(r.get("BlueBalance")).isNotNull().isNotEmpty().isEqualTo("1.457031");
		assertThat(r.get("FocusDistance2")).isNotNull().isNotEmpty().isEqualTo("inf");
		assertThat(r.get("ImageSize")).isNotNull().isNotEmpty().isEqualTo("1024x683");
		assertThat(r.get("LensID")).isNotNull().isNotEmpty().isEqualTo("Sony E 18-55mm F3.5-5.6 OSS");
		assertThat(r.get("RedBalance")).isNotNull().isNotEmpty().isEqualTo("2.480469");
		assertThat(r.get("ScaleFactor35efl")).isNotNull().isNotEmpty().isEqualTo("1.5");
		assertThat(r.get("ShutterSpeed")).isNotNull().isNotEmpty().isEqualTo("1/400");
		assertThat(r.get("ThumbnailImage")).isNotNull().isNotEmpty().isEqualTo("(Binary data 11489 bytes, use -b option to extract)");
		assertThat(r.get("CircleOfConfusion")).isNotNull().isNotEmpty().isEqualTo("0.020 mm");
		assertThat(r.get("FOV")).isNotNull().isNotEmpty().isEqualTo("46.4 deg");
		assertThat(r.get("FocalLength35efl")).isNotNull().isNotEmpty().isEqualTo("28.0 mm (35 mm equivalent: 42.0 mm)");
		assertThat(r.get("HyperfocalDistance")).isNotNull().isNotEmpty().isEqualTo("4.89 m");
		assertThat(r.get("LightValue")).isNotNull().isNotEmpty().isEqualTo("14.6");

	}

	@Test(groups = { "medium", "read-exact" })
	public void testReadAllTagsExact() throws Exception {
		JExifInfo i = tool.getInfo(read04);
		Map<String, String> r = i.getAllExactTagsValues();
		assertThat(r.get("FileName")).isNotNull().isNotEmpty().isEqualTo("read04.JPG");
		assertThat(r.get("FileSize")).isNotNull().isNotEmpty().isEqualTo("245853");
		assertThat(r.get("FileType")).isNotNull().isNotEmpty().isEqualTo("JPEG");
		assertThat(r.get("MIMEType")).isNotNull().isNotEmpty().isEqualTo("image/jpeg");
		assertThat(r.get("ExifByteOrder")).isNotNull().isNotEmpty().isEqualTo("MM");
		assertThat(r.get("Make")).isNotNull().isNotEmpty().isEqualTo("PENTAX");
		assertThat(r.get("Model")).isNotNull().isNotEmpty().isEqualTo("PENTAX K-01");
		assertThat(r.get("Orientation")).isNotNull().isNotEmpty().isEqualTo("1");
		assertThat(r.get("XResolution")).isNotNull().isNotEmpty().isEqualTo("300");
		assertThat(r.get("YResolution")).isNotNull().isNotEmpty().isEqualTo("300");
		assertThat(r.get("ResolutionUnit")).isNotNull().isNotEmpty().isEqualTo("2");
		assertThat(r.get("Software")).isNotNull().isNotEmpty().isEqualTo("K-01 Ver 1.00");
		assertThat(r.get("ModifyDate")).isNotNull().isNotEmpty().isEqualTo("2012:04:28 17:11:13");
		assertThat(r.get("YCbCrPositioning")).isNotNull().isNotEmpty().isEqualTo("2");
		assertThat(r.get("ExposureTime")).isNotNull().isNotEmpty().isEqualTo("0.01666666667");
		assertThat(r.get("FNumber")).isNotNull().isNotEmpty().isEqualTo("3.5");
		assertThat(r.get("ExposureProgram")).isNotNull().isNotEmpty().isEqualTo("2");
		assertThat(r.get("ExifVersion")).isNotNull().isNotEmpty().isEqualTo("0230");
		assertThat(r.get("DateTimeOriginal")).isNotNull().isNotEmpty().isEqualTo("2012:04:28 17:11:13");
		assertThat(r.get("CreateDate")).isNotNull().isNotEmpty().isEqualTo("2012:04:28 17:11:13");
		assertThat(r.get("ComponentsConfiguration")).isNotNull().isNotEmpty().isEqualTo("1 2 3 0");
		assertThat(r.get("Flash")).isNotNull().isNotEmpty().isEqualTo("16");
		assertThat(r.get("FocalLength")).isNotNull().isNotEmpty().isEqualTo("18");
		assertThat(r.get("PentaxVersion")).isNotNull().isNotEmpty().isEqualTo("9 0 0 0");
		assertThat(r.get("PentaxModelType")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("PreviewImageSize")).isNotNull().isNotEmpty().isEqualTo("640 480");
		assertThat(r.get("PreviewImageLength")).isNotNull().isNotEmpty().isEqualTo("37928");
		assertThat(r.get("PreviewImageStart")).isNotNull().isNotEmpty().isEqualTo("5228");
		assertThat(r.get("PentaxModelID")).isNotNull().isNotEmpty().isEqualTo("77560");
		assertThat(r.get("Date")).isNotNull().isNotEmpty().isEqualTo("2012:04:28");
		assertThat(r.get("Time")).isNotNull().isNotEmpty().isEqualTo("17:11:13");
		assertThat(r.get("Quality")).isNotNull().isNotEmpty().isEqualTo("2");
		assertThat(r.get("FlashMode")).isNotNull().isNotEmpty().isEqualTo("1 63");
		assertThat(r.get("FocusMode")).isNotNull().isNotEmpty().isEqualTo("32");
		assertThat(r.get("AFPointSelected")).isNotNull().isNotEmpty().isEqualTo("65531");
		assertThat(r.get("ISO")).isNotNull().isNotEmpty().isEqualTo("6");
		assertThat(r.get("ExposureCompensation")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("MeteringMode")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("AutoBracketing")).isNotNull().isNotEmpty().isEqualTo("0 0");
		assertThat(r.get("WhiteBalance")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("DSPFirmwareVersion")).isNotNull().isNotEmpty().isEqualTo("1 00 00 02");
		assertThat(r.get("EffectiveLV")).isNotNull().isNotEmpty().isEqualTo("9.75");
		assertThat(r.get("ImageEditing")).isNotNull().isNotEmpty().isEqualTo("0 0 0 0");
		assertThat(r.get("PictureMode")).isNotNull().isNotEmpty().isEqualTo("0 0 1");
		assertThat(r.get("DriveMode")).isNotNull().isNotEmpty().isEqualTo("0 0 0 0");
		assertThat(r.get("SensorSize")).isNotNull().isNotEmpty().isEqualTo("24.16 16.016");
		assertThat(r.get("DataScaling")).isNotNull().isNotEmpty().isEqualTo("8192");
		assertThat(r.get("PreviewImageBorders")).isNotNull().isNotEmpty().isEqualTo("28 28 0 0");
		assertThat(r.get("LensType")).isNotNull().isNotEmpty().isEqualTo("4 252");
		assertThat(r.get("SensitivityAdjust")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("ImageEditCount")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("CameraTemperature")).isNotNull().isNotEmpty().isEqualTo("34");
		assertThat(r.get("AELock")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("NoiseReduction")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("FlashExposureComp")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("ImageTone")).isNotNull().isNotEmpty().isEqualTo("1");
		assertThat(r.get("SRResult")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("ShakeReduction")).isNotNull().isNotEmpty().isEqualTo("5");
		assertThat(r.get("SRHalfPressTime")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("SRFocalLength")).isNotNull().isNotEmpty().isEqualTo("18");
		assertThat(r.get("ShutterCount")).isNotNull().isNotEmpty().isEqualTo("283");
		assertThat(r.get("FacesDetected")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("FacePosition")).isNotNull().isNotEmpty().isEqualTo("0 0");
		assertThat(r.get("RawDevelopmentProcess")).isNotNull().isNotEmpty().isEqualTo("10");
		assertThat(r.get("Hue")).isNotNull().isNotEmpty().isEqualTo("1");
		assertThat(r.get("WhiteBalanceAutoAdjustment")).isNotNull().isNotEmpty().isEqualTo("1");
		assertThat(r.get("TungstenAWB")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("DynamicRangeExpansion")).isNotNull().isNotEmpty().isEqualTo("0 2 0 0");
		assertThat(r.get("WorldTimeLocation")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("HometownDST")).isNotNull().isNotEmpty().isEqualTo("1");
		assertThat(r.get("DestinationDST")).isNotNull().isNotEmpty().isEqualTo("1");
		assertThat(r.get("HometownCity")).isNotNull().isNotEmpty().isEqualTo("4");
		assertThat(r.get("DestinationCity")).isNotNull().isNotEmpty().isEqualTo("4");
		assertThat(r.get("HighLowKeyAdj")).isNotNull().isNotEmpty().isEqualTo("0 0");
		assertThat(r.get("FineSharpness")).isNotNull().isNotEmpty().isEqualTo("0 0");
		assertThat(r.get("HighISONoiseReduction")).isNotNull().isNotEmpty().isEqualTo("255 0");
		assertThat(r.get("MonochromeFilterEffect")).isNotNull().isNotEmpty().isEqualTo("65535");
		assertThat(r.get("MonochromeToning")).isNotNull().isNotEmpty().isEqualTo("65535");
		assertThat(r.get("FaceDetect")).isNotNull().isNotEmpty().isEqualTo("0 0");
		assertThat(r.get("FaceDetectFrameSize")).isNotNull().isNotEmpty().isEqualTo("720 478");
		assertThat(r.get("ShadowCorrection")).isNotNull().isNotEmpty().isEqualTo("2 4");
		assertThat(r.get("CrossProcess")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("DistortionCorrection")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("ChromaticAberrationCorrection")).isNotNull().isNotEmpty().isEqualTo("1");
		assertThat(r.get("VignettingCorrection")).isNull();
		assertThat(r.get("BleachBypassToning")).isNotNull().isNotEmpty().isEqualTo("65535");
		assertThat(r.get("AspectRatio")).isNotNull().isNotEmpty().isEqualTo("1");
		assertThat(r.get("HDR")).isNotNull().isNotEmpty().isEqualTo("0 0 0 0");
		assertThat(r.get("BlackPoint")).isNotNull().isNotEmpty().isEqualTo("0 0 0 0");
		assertThat(r.get("WhitePoint")).isNotNull().isNotEmpty().isEqualTo("18304 8192 8192 13280");
		assertThat(r.get("AEExposureTime")).isNotNull().isNotEmpty().isEqualTo("0.0165728151840597");
		assertThat(r.get("AEAperture")).isNotNull().isNotEmpty().isEqualTo("3.66801617281868");
		assertThat(r.get("AE_ISO")).isNotNull().isNotEmpty().isEqualTo("100");
		assertThat(r.get("AEXv")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("AEBXv")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("AEError")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("AEApertureSteps")).isNotNull().isNotEmpty().isEqualTo("1");
		assertThat(r.get("SceneMode")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("AEMaxAperture")).isNotNull().isNotEmpty().isEqualTo("3.5125043207466");
		assertThat(r.get("AEMaxAperture2")).isNotNull().isNotEmpty().isEqualTo("3.5125043207466");
		assertThat(r.get("AEMinAperture")).isNotNull().isNotEmpty().isEqualTo("22.6274169979695");
		assertThat(r.get("AEMinExposureTime")).isNotNull().isNotEmpty().isEqualTo("0.000258950237250933");
		assertThat(r.get("AutoAperture")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("MinAperture")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("LensFStops")).isNotNull().isNotEmpty().isEqualTo("8.5");
		assertThat(r.get("NominalMaxAperture")).isNotNull().isNotEmpty().isEqualTo("3.36358566101486");
		assertThat(r.get("NominalMinAperture")).isNotNull().isNotEmpty().isEqualTo("22.6274169979695");
		assertThat(r.get("MaxAperture")).isNotNull().isNotEmpty().isEqualTo("3.5125043207466");
		assertThat(r.get("ManufactureDate")).isNotNull().isNotEmpty().isEqualTo("2012:02:10");
		assertThat(r.get("ProductionCode")).isNotNull().isNotEmpty().isEqualTo("2.2");
		assertThat(r.get("InternalSerialNumber")).isNotNull().isNotEmpty().isEqualTo("2268");
		assertThat(r.get("PowerSource")).isNotNull().isNotEmpty().isEqualTo("2");
		assertThat(r.get("BodyBatteryState")).isNotNull().isNotEmpty().isEqualTo("5");
		assertThat(r.get("BodyBatteryVoltage1")).isNotNull().isNotEmpty().isEqualTo("7.77");
		assertThat(r.get("BodyBatteryVoltage2")).isNotNull().isNotEmpty().isEqualTo("7.57");
		assertThat(r.get("AFPredictor")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("AFDefocus")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("AFIntegrationTime")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("AFPointsInFocus")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("WBShiftAB")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("WBShiftGM")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("SerialNumber")).isNotNull().isNotEmpty().isEqualTo("4285431");
		assertThat(r.get("SourceDirectoryIndex")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("SourceFileIndex")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("LevelOrientation")).isNotNull().isNotEmpty().isEqualTo("1");
		assertThat(r.get("CompositionAdjust")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("RollAngle")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("PitchAngle")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("CompositionAdjustX")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("CompositionAdjustY")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("CompositionAdjustRotation")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("WB_RGGBLevelsDaylight")).isNotNull().isNotEmpty().isEqualTo("18258 8192 8192 13113");
		assertThat(r.get("WB_RGGBLevelsShade")).isNotNull().isNotEmpty().isEqualTo("21815 8192 8192 9787");
		assertThat(r.get("WB_RGGBLevelsCloudy")).isNotNull().isNotEmpty().isEqualTo("19672 8192 8192 11213");
		assertThat(r.get("WB_RGGBLevelsTungsten")).isNotNull().isNotEmpty().isEqualTo("10972 8192 8192 24327");
		assertThat(r.get("WB_RGGBLevelsFluorescentD")).isNotNull().isNotEmpty().isEqualTo("22544 8192 8192 12638");
		assertThat(r.get("WB_RGGBLevelsFluorescentN")).isNotNull().isNotEmpty().isEqualTo("19329 8192 8192 14064");
		assertThat(r.get("WB_RGGBLevelsFluorescentW")).isNotNull().isNotEmpty().isEqualTo("17572 8192 8192 17390");
		assertThat(r.get("WB_RGGBLevelsFlash")).isNotNull().isNotEmpty().isEqualTo("20701 8192 8192 12068");
		assertThat(r.get("WB_RGGBLevelsFluorescentL")).isNotNull().isNotEmpty().isEqualTo("14700 8192 8192 21761");
		assertThat(r.get("WB_RGGBLevelsUserSelected")).isNotNull().isNotEmpty().isEqualTo("18258 8192 8192 13113");
		assertThat(r.get("ContrastDetectAFArea")).isNotNull().isNotEmpty().isEqualTo("306 203 108 72");
		assertThat(r.get("SensorTemperature")).isNotNull().isNotEmpty().isEqualTo("34.7");
		assertThat(r.get("SensorTemperature2")).isNotNull().isNotEmpty().isEqualTo("34.7");
		assertThat(r.get("FlashpixVersion")).isNotNull().isNotEmpty().isEqualTo("0100");
		assertThat(r.get("ColorSpace")).isNotNull().isNotEmpty().isEqualTo("1");
		assertThat(r.get("ExifImageWidth")).isNotNull().isNotEmpty().isEqualTo("1024");
		assertThat(r.get("ExifImageHeight")).isNotNull().isNotEmpty().isEqualTo("678");
		assertThat(r.get("InteropIndex")).isNotNull().isNotEmpty().isEqualTo("R98");
		assertThat(r.get("InteropVersion")).isNotNull().isNotEmpty().isEqualTo("0100");
		assertThat(r.get("SensingMethod")).isNotNull().isNotEmpty().isEqualTo("2");
		assertThat(r.get("FileSource")).isNotNull().isNotEmpty().isEqualTo("3");
		assertThat(r.get("SceneType")).isNotNull().isNotEmpty().isEqualTo("1");
		assertThat(r.get("CustomRendered")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("ExposureMode")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("FocalLengthIn35mmFormat")).isNotNull().isNotEmpty().isEqualTo("27");
		assertThat(r.get("SceneCaptureType")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("Contrast")).isNotNull().isNotEmpty().isEqualTo("2");
		assertThat(r.get("Saturation")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("Sharpness")).isNotNull().isNotEmpty().isEqualTo("2");
		assertThat(r.get("SubjectDistanceRange")).isNotNull().isNotEmpty().isEqualTo("3");
		assertThat(r.get("GPSVersionID")).isNotNull().isNotEmpty().isEqualTo("2 3 0 0");
		assertThat(r.get("PrintIMVersion")).isNotNull().isNotEmpty().isEqualTo("0300");
		assertThat(r.get("Compression")).isNotNull().isNotEmpty().isEqualTo("6");
		assertThat(r.get("ThumbnailOffset")).isNotNull().isNotEmpty().isEqualTo("54622");
		assertThat(r.get("ThumbnailLength")).isNotNull().isNotEmpty().isEqualTo("7650");
		assertThat(r.get("ImageWidth")).isNotNull().isNotEmpty().isEqualTo("1024");
		assertThat(r.get("ImageHeight")).isNotNull().isNotEmpty().isEqualTo("678");
		assertThat(r.get("EncodingProcess")).isNotNull().isNotEmpty().isEqualTo("0");
		assertThat(r.get("BitsPerSample")).isNotNull().isNotEmpty().isEqualTo("8");
		assertThat(r.get("ColorComponents")).isNotNull().isNotEmpty().isEqualTo("3");
		assertThat(r.get("YCbCrSubSampling")).isNotNull().isNotEmpty().isEqualTo("1 1");
		assertThat(r.get("Aperture")).isNotNull().isNotEmpty().isEqualTo("3.5");
		assertThat(r.get("ImageSize")).isNotNull().isNotEmpty().isEqualTo("1024 678");
		assertThat(r.get("LensID")).isNotNull().isNotEmpty().isEqualTo("4 252");
		assertThat(r.get("PreviewImage")).isNotNull().isNotEmpty().isEqualTo("(Binary data 37928 bytes, use -b option to extract)");
		assertThat(r.get("ScaleFactor35efl")).isNotNull().isNotEmpty().isEqualTo("1.5");
		assertThat(r.get("ShutterSpeed")).isNotNull().isNotEmpty().isEqualTo("0.01666666667");
		assertThat(r.get("ThumbnailImage")).isNotNull().isNotEmpty().isEqualTo("(Binary data 7650 bytes, use -b option to extract)");
		assertThat(r.get("CircleOfConfusion")).isNotNull().isNotEmpty().isEqualTo("0.0200308404192444");
		assertThat(r.get("FOV")).isNotNull().isNotEmpty().isEqualTo("67.3801919655729");
		assertThat(r.get("FocalLength35efl")).isNotNull().isNotEmpty().isEqualTo("27");
		assertThat(r.get("HyperfocalDistance")).isNotNull().isNotEmpty().isEqualTo("4.62144506340791");
		assertThat(r.get("LightValue")).isNotNull().isNotEmpty().isEqualTo("9.52160043943519");

	}

	@Test(groups = { "simple", "read-exact" })
	public void testReadAllSupportedTagsExact() throws Exception {
		JExifInfo i = tool.getInfo(read04);
		Map<Tag, String> r = i.getAllSupportedExactTagsValues();
		assertThat(r).isNotNull().isNotEmpty();
	}

	@Test(groups = { "simple", "read-human" })
	public void testReadAllSupportedTags() throws Exception {
		JExifInfo i = tool.getInfo(read04);
		Map<Tag, String> r = i.getAllSupportedTagsValues();
		assertThat(r).isNotNull().isNotEmpty();
	}

	@Test(groups = { "simple", "read" })
	public void testCustomTagImplementation() throws Exception {
		TagUtil.register(TestTagClass.class);
		String lensSpec = tool.getInfo(read02).getTagValue(TestTagClass.LENSSPEC);
		assertThat(lensSpec).isNotNull().isNotEmpty().isEqualTo("E 18-55mm F3.5-5.6 OSS");
	}

	@Test(groups = { "simple" }, expectedExceptions = { JExifException.class }, expectedExceptionsMessageRegExp = ".*[Please consult the error log].*")
	public void testExifToolNotFound() throws Exception {
		System.setProperty(ExecutionConstant.EXIFTOOLPATH, "bla");
		new JExifTool();
	}

	@Test(groups = { "simple", "write" })
	public void testTimeShift() throws Exception {
		JExifInfo i = tool.getInfo(write02);
		assertThat(i.getTagValue(ExifIFD.CREATEDATE)).isNotNull().isNotEmpty().isEqualTo("2011:11:07 16:16:50");
		i.timeShift(DateTag.CREATEDATE, "1:: 1::");
		assertThat(i.getTagValue(ExifIFD.CREATEDATE)).isNotNull().isNotEmpty().isEqualTo("2012:11:07 17:16:50");
	}

	@Test(groups = { "simple", "write" })
	public void testCopyFromOne() throws Exception {
		JExifInfo i = tool.getInfo(write02);
		i.copyFrom(tool.getInfo(read01), IFD0.MODEL);
		assertThat(i.getTagValue(IFD0.MODEL)).isEqualTo("EX-FH100");
	}

	@Test(groups = { "simple", "write" })
	public void testCopyFromTwo() throws Exception {
		JExifInfo i = tool.getInfo(write02);
		i.copyFrom(tool.getInfo(read01), IFD0.MODEL, ExifIFD.EXPOSUREPROGRAM);
		assertThat(i.getTagValue(IFD0.MODEL)).isEqualTo("EX-FH100");
		assertThat(i.getTagValue(ExifIFD.EXPOSUREPROGRAM)).isEqualTo("Program AE");
	}

	@Test(groups = { "simple", "write" })
	public void testCopyFromAll() throws Exception {
		JExifInfo i = tool.getInfo(write02);
		i.copyFrom(tool.getInfo(read01));
		assertThat(i.getTagValue(IFD0.MODEL)).isEqualTo("EX-FH100");
		assertThat(i.getTagValue(ExifIFD.EXPOSUREPROGRAM)).isEqualTo("Program AE");
	}

	@Test(groups = { "write", "simple" })
	public void testClearAllExifTags() throws Exception {
		JExifInfo i = tool.getInfo(write01);
		Map<String, String> allTagsValuesBefore = i.getAllTagsValues();
		i.deleteAllExifTags();
		Map<String, String> allTagsValuesAfter = i.getAllTagsValues();
		assertThat(allTagsValuesBefore.size()).isGreaterThan(allTagsValuesAfter.size());
		assertThat(allTagsValuesBefore.keySet()).contains(ExifIFD.MAXAPERTUREVALUE.getName(), ExifIFD.FOCALLENGTH.getName());
		assertThat(allTagsValuesAfter.keySet()).excludes(ExifIFD.MAXAPERTUREVALUE.getName(), ExifIFD.FOCALLENGTH.getName());
	}

	@Test(groups = { "simple", "read", "thumbnail" })
	public void testReadThumbnailProvidedWithFormat() throws Exception {
		JExifInfo i = tool.getInfo(read01);
		thumbnail = new File(read01.getCanonicalPath().substring(0, read01.getCanonicalPath().length() - 4) + "_thumbnail.jpg");
		i.extractThumbnail("_thumbnail.jpg");
		assertThat(thumbnail).exists();
	}

	@Test(groups = { "simple", "read", "thumbnail" })
	public void testReadThumbnailProvidedWithoutFormat() throws Exception {
		JExifInfo i = tool.getInfo(read01);
		thumbnail = new File(read01.getCanonicalPath().substring(0, read01.getCanonicalPath().length() - 4) + "_tn.JPG");
		i.extractThumbnail("");
		assertThat(thumbnail).exists();
	}

	@Test(groups = { "simple", "read", "thumbnail" })
	public void testReadThumbnailProvidedWithPath() throws Exception {
		JExifInfo i = tool.getInfo(read01);
		thumbnail = new File("thumbnail_read01.JPG");
		i.extractThumbnail("thumbnail_%f.JPG");
		assertThat(thumbnail).exists();
		assertThat(thumbnail).hasSize(8492L);
	}

	@AfterClass
	public void afterClass() {
	}

	public enum TestTagClass implements Tag {
		LENSSPEC("LensSpec", false, false, false, DataType.STRING);
		private final boolean avoided;
		private final boolean unsafe;
		private final boolean protectedField;
		private final String name;
		private final DataType type;

		private TestTagClass(final String name, final boolean unsafe, final boolean avoided, final boolean protectedField, final DataType type) {
			this.avoided = avoided;
			this.unsafe = unsafe;
			this.protectedField = protectedField;
			this.name = name;
			this.type = type;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see be.pw.jexif.enums.tag.Tag#isAvoided()
		 */
		@Override
		public final boolean isAvoided() {
			return avoided;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see be.pw.jexif.enums.tag.Tag#isUnsafe()
		 */
		@Override
		public final boolean isUnsafe() {
			return unsafe;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see be.pw.jexif.enums.tag.Tag#isProtectedField()
		 */
		@Override
		public final boolean isProtectedField() {
			return protectedField;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see be.pw.jexif.enums.tag.Tag#getName()
		 */
		@Override
		public final String getName() {
			return name;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see be.pw.jexif.enums.tag.Tag#getType()
		 */
		@Override
		public final DataType getType() {
			return type;
		}

	}

}
