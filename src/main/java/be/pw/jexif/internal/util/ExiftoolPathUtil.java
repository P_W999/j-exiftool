/*******************************************************************************
 * Copyright 2014 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.util;

import java.io.File;

import be.pw.jexif.internal.constants.ExecutionConstant;
import lombok.experimental.UtilityClass;

import com.google.common.base.Strings;

/**
 * Class used to find the exiftool executable path.
 *
 * @author phillip
 * @since v0.0.8
 *
 */
@UtilityClass
public class ExiftoolPathUtil {

	/**
	 * Returns a path where the exiftool executable can be found.
	 * <p>
	 * It will first look for a system property {@link ExecutionConstant.EXIFTOOLPATH}, if this returns empty, then an environment variable {@link ExecutionConstant.EXIFTOOLENV} is used to locate the exiftool executable.
	 * <p>
	 * If nothing found, it will default to a windows executable in the current directory.
	 *
	 * @return the exiftool executable path
	 */
	public static File getPath() {
		String property = System.getProperty(ExecutionConstant.EXIFTOOLPATH);
		if (!Strings.isNullOrEmpty(property)) {
			return new File(property);
		}
		String env = System.getenv(ExecutionConstant.EXIFTOOLENV);
		if (!Strings.isNullOrEmpty(env)) {
			return new File(env);
		}
		return new File(".\\exiftool.exe");
	}
}
