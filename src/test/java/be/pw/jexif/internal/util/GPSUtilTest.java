/*******************************************************************************
 * Copyright 2013 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.util;

import static org.fest.assertions.Assertions.assertThat;

import org.testng.annotations.Test;

public class GPSUtilTest {

	@Test
	public void DMSToDecimal() {
		assertThat(GPSUtil.dmsToDecimal("87 43 41")).isEqualTo("87.72805555555556");
	}
}
