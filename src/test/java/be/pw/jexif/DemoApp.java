/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import be.pw.jexif.enums.DateTag;
import be.pw.jexif.enums.tag.ExifIFD;
import be.pw.jexif.enums.tag.Tag;
import be.pw.jexif.exception.ExifError;
import be.pw.jexif.exception.JExifException;
import be.pw.jexif.internal.constants.ExecutionConstant;
import be.pw.jexif.internal.util.TimeShiftGenerator;

import com.google.common.base.Stopwatch;
import com.google.common.io.Files;

/**
 * The main class file.
 *
 * @author phillip
 */
class DemoApp {

	private static final Logger LOG = LoggerFactory.getLogger(DemoApp.class);

	/**
	 * Just a simple test main method that is used during development.
	 *
	 * @param args arguments
	 * @throws IOException
	 * @throws ExifError
	 */
	public static void main(final String[] args) throws IOException, JExifException, InterruptedException, ExifError {
		LOG.info("Starting main method");
		Properties p = new Properties();
		p.load(DemoApp.class.getResourceAsStream("/jexiftool.properties"));
		LOG.trace("Using version {}", p.get("jexiftool.version"));

		System.setProperty(ExecutionConstant.EXIFTOOLPATH, p.getProperty(ExecutionConstant.EXIFTOOLPATH));
		System.setProperty(ExecutionConstant.EXIFTOOLDEADLOCK, p.getProperty(ExecutionConstant.EXIFTOOLDEADLOCK));
		Stopwatch watch = Stopwatch.createStarted();
		JExifTool tool = new JExifTool();
		JExifInfo info1 = tool.getInfo(new File("src/test/resources/read01.JPG"));
		JExifInfo info2 = tool.getInfo(new File("src/test/resources/read02.JPG"));
		JExifInfo info3 = tool.getInfo(new File("src/test/resources/read03.JPG"));
		JExifInfo info4 = tool.getInfo(new File("src/test/resources/read04.JPG"));

		File thumbnail = new File("src/test/resources/read01_tn.jpg");
		if (thumbnail.exists()) {
			thumbnail.delete();
		}
		thumbnail.deleteOnExit();

		File tmp = File.createTempFile("jexiftool", ".jpg");
		File gps = File.createTempFile("gpstest", ".jpg");
		File shift = File.createTempFile("gpstest", ".jpg");
		File copyFrom = File.createTempFile("copyfrom", ".jpg");
		File toDeleteAllFrom = File.createTempFile("deletAll", "jpg");
		tmp.deleteOnExit();
		gps.deleteOnExit();
		shift.deleteOnExit();
		copyFrom.deleteOnExit();
		toDeleteAllFrom.deleteOnExit();
		Files.copy(new File("src/test/resources/read04.JPG"), tmp);
		Files.copy(new File("src/test/resources/read04.JPG"), gps);
		Files.copy(new File("src/test/resources/read04.JPG"), shift);
		Files.copy(new File("src/test/resources/read04.JPG"), copyFrom);
		Files.copy(new File("src/test/resources/read04.JPG"), toDeleteAllFrom);

		JExifInfo deletAll = tool.getInfo(toDeleteAllFrom);
		deletAll.deleteAllExifTags();

		Map<Tag, String> allSupportedTagsValues = deletAll.getAllSupportedTagsValues();
		for (Entry<Tag, String> entry : allSupportedTagsValues.entrySet()) {
			LOG.info("Leftover: " + entry.getKey().getName() + ":" + entry.getValue());
		}

		// tool.stop();
		JExifInfo write1 = tool.getInfo(tmp);

		info1.extractThumbnail("_tn.jpg");
		LOG.info("Thumbnail exists ? " + thumbnail.exists());

		LOG.info("FNUMBER value is for read 1 = {}", info1.getTagValue(ExifIFD.FNUMBER));
		LOG.info("FNUMBER value is for read 1 = {}", info1.getTagValue(ExifIFD.APERTUREVALUE));
		info1.setTagValue(ExifIFD.FNUMBER, "13.0");
		info1.setTagValue(ExifIFD.APERTUREVALUE, "13.0");
		LOG.info("FNUMBER value is for read 1 = {}", info1.getTagValue(ExifIFD.FNUMBER));
		LOG.info("FNUMBER value is for read 1 = {}", info1.getTagValue(ExifIFD.APERTUREVALUE));

		LOG.info("ISO value is for read 1 = {}", info1.getTagValue(ExifIFD.ISO));
		LOG.info("ISO value is for read 2 = {}", info2.getTagValue(ExifIFD.ISO));
		LOG.info("FNUMBER value is for read 1 = {}", info1.getTagValue(ExifIFD.FNUMBER));
		LOG.info("EXPOSURE_PROGRAM value is for read 1 = {}", info1.getTagValue(ExifIFD.EXPOSUREPROGRAM));
		LOG.info("EXPOSURE_PROGRAM exact value is for read 1 = {}", info1.getExactTagValue(ExifIFD.EXPOSUREPROGRAM));
		LOG.info("EXPOSURE_TIME value is for read 2 = {}", info2.getTagValue(ExifIFD.EXPOSURETIME));
		LOG.info("EXPOSURE_TIME exact value is for read 2 = {}", info2.getExactTagValue(ExifIFD.EXPOSURETIME));
		LOG.info("FNUMBER exact value is for read 1 = {}", info1.getExactTagValue(ExifIFD.FNUMBER));
		LOG.info("FOCAL_LENGTH exact value is for read 4 = {}", info4.getExactTagValue(ExifIFD.FOCALLENGTH));
		LOG.info("FOCAL_LENGTH exact value is for read 3 = {}", info3.getExactTagValue(ExifIFD.FOCALLENGTH));

		LOG.info("FOCAL_LENGTH value is for read 4 = {}", info4.getTagValue(ExifIFD.FOCALLENGTH));
		LOG.info("Writing 5 to FOCAL_LENGTH for write 1 -> {}", write1.setTagValue(ExifIFD.FOCALLENGTH, "5.0"));
		LOG.info("FOCAL_LENGTH value is for write 1 = {}", write1.getTagValue(ExifIFD.FOCALLENGTH));
		Map<String, String> all = info2.getAllTagsValues();
		for (Entry<String, String> e : all.entrySet()) {
			LOG.info("ALL: {} : {}", e.getKey(), e.getValue());
		}
		Map<String, String> allExact = info2.getAllExactTagsValues();
		for (Entry<String, String> e : allExact.entrySet()) {
			LOG.info("ALLEXACT: {} : {}", e.getKey(), e.getValue());
		}
		watch.stop();

		try {
			tool.getInfo(new File("c:\\does\\not\\exist.pdf"));
		} catch (IOException e1) {
			LOG.error(e1.getMessage());
		}
		tool.stop();
		/*
		 * IMessageConveyor mc = new MessageConveyor(Locale.ENGLISH); System.out.println(mc.getMessage(Errors.GENERAL, "test")); mc = new MessageConveyor(new Locale("nl")); System.out.println(mc.getMessage(Errors.GENERAL, "test")); System.out.println(Cal10nUtil.get(Errors.GENERAL, "test"));
		 */

		JExifInfo gpsInfo = tool.getInfo(gps);
		// 50.84791165546716N 4.350469368264016E == Bourse @ Brussels
		// gpsInfo.setGPSInfo("50.84791165546716", "N", "4.350469368264016", "E", "50", "0");

		// 60.586967S 131.835938W == South Pacific Ocean
		gpsInfo.setGPSInfo("60 35 13.08", "S", "131 50 9.38", "W", "100", "1");
		tool.stop();

		/*
		 * System.out.println(Cal10nUtil.get(Errors.VALIDATION_RAT_FOR_INT)); Cal10nUtil.changeLocale(Locale.ENGLISH); System.out.println(Cal10nUtil.get(Errors.VALIDATION_RAT_FOR_INT)); Cal10nUtil.changeLocale(new Locale("nl")); System.out.println(Cal10nUtil.get(Errors.VALIDATION_RAT_FOR_INT));
		 */
		JExifInfo shiftWrite = tool.getInfo(shift);
		LOG.info("Original timestamp: " + shiftWrite.getTagValue(ExifIFD.CREATEDATE));
		LOG.info("Shifted timestamp: " + shiftWrite.timeShift(DateTag.CREATEDATE, TimeShiftGenerator.generateTimeShift(true, 1, 0, 0)));

		JExifInfo copyFromWrite = tool.getInfo(copyFrom);
		LOG.info("Original EXPOSURETIME: " + shiftWrite.getTagValue(ExifIFD.EXPOSURETIME));
		LOG.info("To be EXPOSURETIME: " + info1.getTagValue(ExifIFD.EXPOSURETIME));
		copyFromWrite.copyFrom(info1);
		LOG.info("New EXPOSURETIME: " + copyFromWrite.getTagValue(ExifIFD.EXPOSURETIME));

		LOG.info("Executing took {} ms", watch.elapsed(TimeUnit.MILLISECONDS));
		LOG.info("Finished main method");
	}

}
