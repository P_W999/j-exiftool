/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.util;

import java.util.HashMap;
import java.util.Map;

import be.pw.jexif.enums.DataType;
import be.pw.jexif.enums.Errors;
import be.pw.jexif.enums.tag.Tag;
import be.pw.jexif.exception.JExifValidationException;
import be.pw.jexif.internal.constants.ExecutionConstant;

/**
 * General utility class for working with Tags.
 */
public final class TagUtil {

	/**
	 * Hidden constructor.
	 */
	private TagUtil() {
	}

	/**
	 * Look-up map for quick String-to-Enum conversion.
	 */
	private static final Map<String, Tag> TAG_LOOKUP_MAP = new HashMap<>(246); // at v0.0.3, the supported tag count is 246
	/**
	 * The minimum value for an Unsigned Int8.
	 */
	private static final Integer MIN_INT8U = -128;
	/**
	 * The maximum value for an Unsigned Int8.
	 */
	private static final Integer MAX_INT8S = 127;
	/**
	 * The maximum value for a Signed Int8.
	 */
	private static final Integer MAX_INT8U = 255;
	/**
	 * The maximum value for an Unsigned Int16.
	 */
	private static final Long MAX_INT16U = 65535L;
	/**
	 * The maximum value for an Unsigned Int32.
	 */
	private static final Float MAX_INT32U = 4294967295F;

	/**
	 * Registers an implementation of {@link Tag} .<br />
	 * Registering a Tag implementation allows J-ExifTool to parse it's value correctly from the ExifTool output.
	 *
	 * @param tagClass the class to register.
	 */
	public static void register(final Class<? extends Tag> tagClass) {
		Tag[] values = tagClass.getEnumConstants();

		for (Tag tag : values) {
			TAG_LOOKUP_MAP.put(tag.getName(), tag);
		}
	}

	/**
	 * Used to get the {@link Tag} identified by the given, case-sensitive, tag name.
	 *
	 * @param name The case-sensitive name of the tag that will be searched for.
	 * @return the {@link Tag} identified by the given, case-sensitive, tag name or <code>null</code> if one couldn't be found.
	 */
	public static Tag forName(final String name) {
		return TAG_LOOKUP_MAP.get(name);
	}

	/**
	 * Validates that a given value has to correct format based on the data type of the tag.
	 *
	 * @see DataType
	 * @param tag the tag to which the value shall be written.
	 * @param value the value to be written.
	 * @throws JExifValidationException if the given value is not formatted correctly.
	 */
	public static void validateValue(final Tag tag, final String value) throws JExifValidationException {
		if (Boolean.getBoolean(ExecutionConstant.EXIFTOOLBYPASSVALIDATION)) {
			return;
		}
		DataType type = tag.getType();
		Exception ex = null;
		Errors error = null;
		switch (type) {
			case FLOAT:
				try {
					Float.parseFloat(value);
				} catch (NumberFormatException e) {
					error = Errors.VALIDATION_INVALID_TYPE;
					ex = e;
				}
				break;
			case INT16S:
				try {
					Short.parseShort(value);
				} catch (NumberFormatException e) {
					error = Errors.VALIDATION_INVALID_TYPE;
					ex = e;
				}
				break;
			case INT16U:
				try {
					Integer l = Integer.parseInt(value);
					if (l < 0) {
						error = Errors.VALIDATION_NEGATIVE_UNSIGNED;
						break;
					}
					if (l > MAX_INT16U) {
						error = Errors.VALIDATION_EXCEEDS_LIMITS;
						break;
					}
				} catch (NumberFormatException e) {
					error = Errors.VALIDATION_INVALID_TYPE;
					ex = e;
				}
			case INT32S:
				try {
					Integer.parseInt(value);
				} catch (NumberFormatException e) {
					error = Errors.VALIDATION_INVALID_TYPE;
					ex = e;
				}
				break;
			case INT32U:
				try {
					Long l = Long.parseLong(value);
					if (l < 0) {
						error = Errors.VALIDATION_NEGATIVE_UNSIGNED;
						break;
					}
					if (l > MAX_INT32U) {
						error = Errors.VALIDATION_EXCEEDS_LIMITS;
						break;
					}
				} catch (NumberFormatException e) {
					error = Errors.VALIDATION_INVALID_TYPE;
					ex = e;
				}
			case INT8S:
				try {
					Short s = Short.parseShort(value);
					if (s > MAX_INT8S || s < MIN_INT8U) {
						error = Errors.VALIDATION_EXCEEDS_LIMITS;
						break;
					}
				} catch (NumberFormatException e) {
					error = Errors.VALIDATION_INVALID_TYPE;
					ex = e;
				}
			case INT8U:
				try {
					Short s = Short.parseShort(value);
					if (s < 0) {
						error = Errors.VALIDATION_NEGATIVE_UNSIGNED;
						break;
					}
					if (s > MAX_INT8U) {
						error = Errors.VALIDATION_EXCEEDS_LIMITS;
						break;
					}
				} catch (NumberFormatException e) {
					error = Errors.VALIDATION_INVALID_TYPE;
					ex = e;
				}
			case RAT64S:
				try {
					Double.parseDouble(value);
				} catch (NumberFormatException e) {
					error = Errors.VALIDATION_INVALID_TYPE;
					ex = e;
				}
			case RAT64U:
				try {
					Double d = Double.parseDouble(value);
					if (d < 0) {
						error = Errors.VALIDATION_NEGATIVE_UNSIGNED;
						break;
					}
				} catch (NumberFormatException e) {
					// There is the possibility that the value is too big to fit in a Double
					int l = Double.toString(Double.MAX_VALUE).length();
					if (value.length() < l) { // not valid number
						error = Errors.VALIDATION_INVALID_TYPE;
						ex = e;
						break;
					} else if (value.length() == l) { // maybe it's too big
						String shorterValue = value.substring(0, l - 2);
						try {
							Double.parseDouble(shorterValue);
						} catch (NumberFormatException e1) { // it wasn't too big after all
							error = Errors.VALIDATION_INVALID_TYPE;
							ex = e;
							break;
						}
					} else {
						break;
					}
				}
			case STRING:
				break;
			case UNDEF:
				break;
			default:
				throw new IllegalArgumentException(Cal10nUtil.get(Errors.UNKNOWN_DATATYPE, type.toString()));
		}
		if (error != null) {
			throw new JExifValidationException(Cal10nUtil.get(error, tag.getName(), type, value), ex);
		}
	}

}
