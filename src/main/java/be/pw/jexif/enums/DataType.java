/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.enums;

/**
 * Defines the data-type a tag value.<br />
 * Supported types are:
 * <ul>
 * <li>Short signed</li>
 * <li>Short unsigned</li>
 * <li>Integer signed</li>
 * <li>Integer unsigned</li>
 * <li>Long signed</li>
 * <li>Long unsigned</li>
 * <li>Double signed</li>
 * <li>Double unsigned (unsupported by Java)</li>
 * <li>String</li>
 * <li>Float</li>
 * <li>Undefined for any other case</li>
 * </ul>
 *
 * @author phillip
 */
public enum DataType {

	INT8S, INT8U, INT16U, INT16S, INT32U, INT32S, RAT64U, RAT64S, STRING, FLOAT, UNDEF
}
