/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.action;

import java.io.File;
import java.util.Map;

import be.pw.jexif.enums.tag.Tag;
import be.pw.jexif.exception.JExifException;

/**
 * An operation that should be executed by ExifTool is called an action. Eg: reading a tag in a human readable format is an action. Reading a tag in the exact format is another action.<br />
 * A typical life-cycle of an action:
 * <ol>
 * <li>Create a new instance of the action</li>
 * <li>Assign a unique id to the action</li>
 * <li>Set parameters if required</li>
 * <li>Create the arguments for ExifTool</li>
 * <li>Wait for results to be returned by the {@link be.pw.jexif.internal.result.ResultHandler}
 * </ol>
 * The IAction interface describes such action.<br />
 * An action may only be instantiated by the {@link be.pw.jexif.internal.action.impl.ActionFactory} and for each action an {@link be.pw.jexif.internal.result.IResultParser} should be created.
 *
 * @author phillip
 */
public interface IAction {

	/**
	 * Returns the id of the action.
	 *
	 * @return the id (but never returns null)
	 */
	String getId();

	/**
	 * Generates the array of arguments to pass to ExifTool.
	 *
	 * @return an array of arguments
	 * @throws JExifException in case of an exception (most likely IO)
	 */
	String[] buildArguments() throws JExifException;

	/**
	 * Returns the result of the execution as a String.
	 *
	 * @return the map with as key the Tag and as value the result of the execution
	 */
	Map<Tag, String> getResult();

	/**
	 * Adds the result for a Tag.
	 *
	 * @param tag the tag for which the result is added
	 * @param result the result of the action
	 */
	void addResult(Tag tag, String result);

	/**
	 * Returns the list of tags that were read but are not supported by J-ExifTool.
	 *
	 * @return the list of unsupported tags.
	 */
	Map<String, String> getUnsupportedTags();

	/**
	 * Adds the result for an unsupported tag.
	 *
	 * @param tagName the name of the unsupported tag (as returned by ExifTool)
	 * @param result the result
	 */
	void addUnsupportedResult(final String tagName, final String result);

	/**
	 * Returns the file on which this action will apply.
	 *
	 * @return the file property
	 */
	File getFile();
}
