/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.thread.event;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * An event which can be posted to the EventBus which can trigger the proper EventHandler.
 *
 * @author phillip
 * @see be.pw.jexif.internal.thread.event.EventHandler
 * @see com.google.common.eventbus.EventBus
 */
@AllArgsConstructor
@Getter(value = AccessLevel.PROTECTED)
public class ErrorEvent {
	/**
	 * The message which shall be posted to the EventBus.
	 */
	private final String msg;

}
