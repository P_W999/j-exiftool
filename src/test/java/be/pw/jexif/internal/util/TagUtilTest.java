/*******************************************************************************
 * Copyright 2013 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.util;

import org.testng.annotations.Test;

import be.pw.jexif.enums.tag.IFD0;
import be.pw.jexif.exception.JExifValidationException;
import be.pw.jexif.internal.constants.ExecutionConstant;

public class TagUtilTest {

	@Test(expectedExceptions = { JExifValidationException.class }, expectedExceptionsMessageRegExp = ".*[could not be parsed as being such].*")
	public void validateValueWithoutSystemProperty() throws JExifValidationException {
		System.clearProperty(ExecutionConstant.EXIFTOOLBYPASSVALIDATION);
		TagUtil.validateValue(IFD0.IMAGEWIDTH, "haha");
	}

	@Test(expectedExceptions = { JExifValidationException.class }, expectedExceptionsMessageRegExp = ".*[could not be parsed as being such].*")
	public void validateValueWithExpliciteEnable() throws JExifValidationException {
		System.setProperty(ExecutionConstant.EXIFTOOLBYPASSVALIDATION, "false");
		TagUtil.validateValue(IFD0.IMAGEWIDTH, "haha");
	}

	@Test
	public void validateValueWithExpliciteDisable() throws JExifValidationException {
		System.setProperty(ExecutionConstant.EXIFTOOLBYPASSVALIDATION, "true");
		TagUtil.validateValue(IFD0.IMAGEWIDTH, "haha");
	}
}
