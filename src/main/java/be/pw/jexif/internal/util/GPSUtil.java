/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.util;

import java.util.Map;

import com.google.common.annotations.VisibleForTesting;

import be.pw.jexif.enums.Errors;
import be.pw.jexif.enums.tag.ExifGPS;
import be.pw.jexif.enums.tag.Tag;
import be.pw.jexif.exception.JExifValidationException;
import lombok.experimental.UtilityClass;

/**
 * General utility class to work with GPS tags.
 *
 * @author phillip
 */
@UtilityClass
public final class GPSUtil {

	/**
	 * Validates that the all mandatory GPS values are provided or that they are all empty. The Map must therefore contain the following Tags:
	 * <ul>
	 * <li>{@link ExifGPS#GPSALTITUDE}</li>
	 * <li>{@link ExifGPS#GPSALTITUDEREF}</li>
	 * <li>{@link ExifGPS#GPSLONGITUDE}</li>
	 * <li>{@link ExifGPS#GPSLONGITUDEREF}</li>
	 * <li>{@link ExifGPS#GPSLATITUDE}</li>
	 * <li>{@link ExifGPS#GPSLATITUDEREF}</li>
	 * </ul>
	 *
	 * @param valuesToValidate the map with the GPS values.
	 * @throws JExifValidationException if a value is missing.
	 */
	public static void validateGPSValues(final Map<Tag, String> valuesToValidate) throws JExifValidationException {
		boolean allEmpty = true;
		boolean allFilledIn = true;
		allEmpty = valuesToValidate.get(ExifGPS.GPSALTITUDE) == null && valuesToValidate.get(ExifGPS.GPSALTITUDEREF) == null && valuesToValidate.get(ExifGPS.GPSLONGITUDE) == null && valuesToValidate.get(ExifGPS.GPSLONGITUDEREF) == null && valuesToValidate.get(ExifGPS.GPSLATITUDE) == null && valuesToValidate.get(ExifGPS.GPSLATITUDEREF) == null;
		allFilledIn = valuesToValidate.get(ExifGPS.GPSALTITUDE) != null && valuesToValidate.get(ExifGPS.GPSALTITUDEREF) != null && valuesToValidate.get(ExifGPS.GPSLONGITUDE) != null && valuesToValidate.get(ExifGPS.GPSLONGITUDEREF) != null && valuesToValidate.get(ExifGPS.GPSLATITUDE) != null && valuesToValidate.get(ExifGPS.GPSLATITUDEREF) != null;

		if (!allEmpty && !allFilledIn) {
			throw new JExifValidationException(Cal10nUtil.get(Errors.GPS_MISSING_FIELD));
		}
	}

	/**
	 * Converts the provided GPS values to decimal format. The following Tags will be converted from DMS format to decimal format if needed:
	 * <ul>
	 * <li>{@link ExifGPS#GPSLONGITUDE}</li>
	 * <li>{@link ExifGPS#GPSLATITUDE}</li>
	 * </ul>
	 *
	 * @param valuesToAlter the Map with GPS values.
	 */
	public static void formatGPSValues(final Map<Tag, String> valuesToAlter) {
		String lon = valuesToAlter.get(ExifGPS.GPSLONGITUDE);
		String lat = valuesToAlter.get(ExifGPS.GPSLATITUDE);
		if (lon != null && lon.trim().contains(" ")) {
			lon = dmsToDecimal(lon);
			valuesToAlter.put(ExifGPS.GPSLONGITUDE, lon);
		}
		if (lat != null && lat.trim().contains(" ")) {
			lat = dmsToDecimal(lat);
			valuesToAlter.put(ExifGPS.GPSLATITUDE, lat);
		}

	}

	/**
	 * Converts DMS GPS format to decimal GPS format.
	 *
	 * @param dms the longitude or latitude in DMS format
	 * @return the longitude or latitude in decimal format.
	 */
	@VisibleForTesting
	static String dmsToDecimal(final String dms) {
		String[] split = dms.split("\\s");
		Double d = 0.0;
		Double m = 0.0;
		Double s = 0.0;
		Double decimal = 0.0;
		if (split.length >= 1) {
			d = Double.parseDouble(split[0]);
		}
		if (split.length >= 2) {
			m = Double.parseDouble(split[1]);
		}
		if (split.length >= 3) {
			s = Double.parseDouble(split[2]);
		}
		decimal = d + (((60 * m) + s) / 3600.0);
		return String.format("%.14f", decimal).replace(",", ".");

	}
}
