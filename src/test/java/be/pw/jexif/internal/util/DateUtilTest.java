/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.util;

import static org.fest.assertions.Assertions.assertThat;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DateUtilTest {
	Calendar c;

	@BeforeMethod(alwaysRun = true)
	public void before() {
		c = new GregorianCalendar(TimeZone.getTimeZone("GMT+01:00"));
		c.set(2000, Calendar.JANUARY, 25, 18, 12, 5);
		c.set(Calendar.MILLISECOND, 55);
	}

	@Test
	public void fromDate() {
		assertThat(DateUtil.fromDate(c.getTime())).isEqualTo("2000:01:25 18:12:05.55 +01:00");
	}

	@Test
	public void toDate() throws Exception {
		Date d1 = DateUtil.toDate("2000:01:25 18:12:05.55 +01:00");
		Calendar c1 = new GregorianCalendar();
		c1.setTime(d1);
		assertThat(c.equals(c1));
	}

	@Test(dependsOnMethods = { "fromDate", "toDate" })
	public void testEquality() throws Exception {
		String s1 = DateUtil.fromDate(c.getTime());
		Date d2 = DateUtil.toDate(s1);
		Calendar c2 = new GregorianCalendar();
		c2.setTime(d2);
		assertThat(c2.equals(c));
	}

	@Test(groups = { "yearEnding" }, dependsOnMethods = { "fromDate" })
	public void testYearEndingFromDateNewYear() {
		c.set(Calendar.YEAR, 2015);
		c.set(Calendar.MONTH, Calendar.JANUARY);
		c.set(Calendar.DAY_OF_MONTH, 1);
		assertThat(DateUtil.fromDate(c.getTime())).isEqualTo("2015:01:01 18:12:05.55 +01:00");
	}

	@Test(groups = { "yearEnding" }, dependsOnMethods = { "fromDate" })
	public void testYearEndingFromDateOldYear() {
		c.set(Calendar.YEAR, 2014);
		c.set(Calendar.MONTH, Calendar.DECEMBER);
		c.set(Calendar.DAY_OF_MONTH, 31);
		assertThat(DateUtil.fromDate(c.getTime())).isEqualTo("2014:12:31 18:12:05.55 +01:00");
	}

	@Test(groups = { "yearEnding" }, dependsOnMethods = { "toDate" })
	public void testYearEndingToDateNewYear() throws ParseException {
		Date d1 = DateUtil.toDate("2015:01:01 18:12:05.55 +01:00");
		Calendar c1 = new GregorianCalendar();
		c1.setTime(d1);
		c.set(Calendar.YEAR, 2015);
		c.set(Calendar.MONTH, Calendar.JANUARY);
		c.set(Calendar.DAY_OF_MONTH, 1);
		assertThat(c1.equals(c));
	}

	@Test(groups = { "yearEnding" }, dependsOnMethods = { "toDate" })
	public void testYearEndingToDateOldYear() throws ParseException {
		Date d1 = DateUtil.toDate("2014:12:31 18:12:05.55 +01:00");
		Calendar c1 = new GregorianCalendar();
		c1.setTime(d1);
		c.set(Calendar.YEAR, 2014);
		c.set(Calendar.MONTH, Calendar.DECEMBER);
		c.set(Calendar.DAY_OF_MONTH, 31);
		assertThat(c1.equals(c));
	}

}
