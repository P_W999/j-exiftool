/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.result;

import java.util.ArrayList;
import java.util.List;

import be.pw.jexif.enums.Errors;
import be.pw.jexif.exception.ExifError;
import be.pw.jexif.exception.JExifException;
import be.pw.jexif.internal.action.IAction;
import be.pw.jexif.internal.result.impl.ParserFactory;
import be.pw.jexif.internal.util.Cal10nUtil;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

/**
 * The ResultHandler will be used to run over the different results (lines returned by ExifTool) in order to match the output with the input.
 *
 * @author phillip
 */
@Slf4j
@UtilityClass
public final class ResultHandler {

	/**
	 * Threats the input- and error-messages for the given action.<br />
	 * A different result handler will be used based on the type of action passed to this method.
	 *
	 * @param action the action
	 * @param result the result-list for the given action.
	 * @param errors the error-list for the given action.
	 * @throws ExifError if the error-list is not empty (the ExifError will contain the error-messages).
	 */
	public static void run(final IAction action, final List<String> result, final List<String> errors) throws ExifError {
		if (errors != null && !errors.isEmpty()) {
			boolean onlyWarnings = true;
			StringBuilder b = new StringBuilder();
			b.append(Cal10nUtil.get(Errors.EXIFTOOL_EXECUTION_WARNING));
			for (String s : errors) {
				if (!s.toLowerCase().contains("warning")) {
					onlyWarnings = false;
					b.append(" > ").append(s);
				} else {
					log.warn(s);
				}
			}
			if (!onlyWarnings) {
				log.error(Cal10nUtil.get(Errors.EXIFTOOL_EXECUTION_ERROR, b.toString()));
				throw new ExifError(b.toString());
			}
		}
		IResultParser parser;

		String uid = action.getId();
		log.trace("[{}] - Getting results", uid);
		if (result != null && !result.isEmpty()) {
			try {
				log.trace("[{}] - {} results were found", uid, result.size());
				parser = ParserFactory.getInstance(action);
				log.trace("[{}] - Parser {} was retreived", uid, parser.getClass().getSimpleName());
				List<String> fixedResult = new ArrayList<>(); // quick fix for concurrent modification exception
				fixedResult.addAll(result);
				parser.parse(action, uid, fixedResult);
			} catch (JExifException e) {
				log.debug(Cal10nUtil.get(Errors.RETREIVE_PARSER_ERROR, uid), e);
				log.warn("[{}] - There will be no results due to an exception", uid);
			}
		} else {
			log.warn("[{}] - No results were found", uid);
		}
	}

}
