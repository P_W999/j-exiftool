/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.result.impl;

import static org.fest.assertions.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import be.pw.jexif.enums.tag.ExifGPS;
import be.pw.jexif.enums.tag.ExifIFD;
import be.pw.jexif.enums.tag.IFD0;
import be.pw.jexif.enums.tag.Tag;
import be.pw.jexif.internal.action.IAction;
import be.pw.jexif.internal.action.impl.ActionFactory;
import be.pw.jexif.internal.util.TagUtil;

public class TagReadParserTest {

	TagReadParser parser;
	IAction actionForTest;
	List<String> results;

	@BeforeTest
	public void setUp() throws IOException {
		parser = new TagReadParser();
		actionForTest = ActionFactory.createReadAction(new File("."), ExifIFD.APERTUREVALUE, ExifIFD.EXPOSURETIME);
		results = new ArrayList<String>();
		TagUtil.register(IFD0.class);
		TagUtil.register(ExifIFD.class);
		TagUtil.register(ExifGPS.class);
	}

	@Test
	public void testParse() throws Exception {
		String uid = actionForTest.getId();
		results.add(ExifIFD.EXPOSURETIME.getName() + ":    3.4");
		results.add(ExifIFD.APERTUREVALUE.getName() + ":    4.5");
		parser.parse(actionForTest, uid, results);
		assertThat(actionForTest.getResult()).isNotNull().isNotEmpty();
		Map<Tag, String> result = actionForTest.getResult();
		assertThat(result.size()).isEqualTo(2);
		for (Entry<Tag, String> t : result.entrySet()) {
			String s = t.getValue();
			if (s.equals("3.4") || s.equals("4.5")) {
				assertThat(true).isTrue();
			} else {
				org.testng.Assert.fail("Result was none of the expected values");
			}
		}
	}
}
