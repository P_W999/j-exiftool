/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.enums;

import ch.qos.cal10n.BaseName;
import ch.qos.cal10n.Locale;
import ch.qos.cal10n.LocaleData;

/**
 * Enum which is used in combination with cal10n to i18n the errormessages.
 *
 * @see be.pw.jexif.internal.util.Cal10nUtil
 */
@BaseName("errorMessages")
@LocaleData({ @Locale("en") })
public enum Errors {
	EXIFTOOL_INVALID_PATH, GENERAL, EXIFTHREAD, IO_ARGSFILE, EXC_CLOSING, INTERRUPTED_SLEEP, DEADLOCK, VALIDATION_TAGLIST, IO_BUILD_ARGUMENTS, VALIDATION_VALUESLIST, EXIFTOOL_EXECUTION_ERROR, EXIFTOOL_EXECUTION_WARNING, RETREIVE_PARSER_ERROR, PARSER_UNKNOWNTYPE, PARSER_MISMATCH, VALIDATION_WRITE_PROTECTED_TAG, VALIDATION_WRITE_AVOIDED_TAG, VALIDATION_WRITE_UNSAFE_TAG, UNKNOWN_DATATYPE, VALIDATION_NEGATIVE_UNSIGNED, VALIDATION_EXCEEDS_LIMITS, VALIDATION_RAT_FOR_INT, VALIDATION_INVALID_TYPE, IO_FILE_NOT_VALID, GPS_MISSING_FIELD, VALIDATION_TIMESHIFT
}
