/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.enums.tag;

import static be.pw.jexif.enums.DataType.INT16S;
import static be.pw.jexif.enums.DataType.INT16U;
import static be.pw.jexif.enums.DataType.INT32S;
import static be.pw.jexif.enums.DataType.INT32U;
import static be.pw.jexif.enums.DataType.INT8U;
import static be.pw.jexif.enums.DataType.RAT64S;
import static be.pw.jexif.enums.DataType.RAT64U;
import static be.pw.jexif.enums.DataType.STRING;
import static be.pw.jexif.enums.DataType.UNDEF;
import be.pw.jexif.enums.DataType;
import lombok.Getter;

/**
 * Implementation of the ExifIFD group: <a href="http://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/EXIF.html">http://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/EXIF.html</a>.
 *
 * @author phillip
 */
@Getter
public enum ExifIFD implements Tag {

	APERTUREVALUE("ApertureValue", false, false, false, RAT64U), APPLICATIONNOTES("ApplicationNotes", true, false, false, INT8U), BRIGHTNESS("Brightness", false, true, false, STRING), BRIGHTNESSVALUE("BrightnessValue", false, false, false, RAT64S), CFAPATTERN("CFAPattern", false, false, false, UNDEF), COLORSPACE("ColorSpace", false, false, false, INT16U), COMPONENTSCONFIGURATION("ComponentsConfiguration", true, false, false, UNDEF), COMPRESSEDBITSPERPIXEL("CompressedBitsPerPixel", true, false, false, RAT64U), CONTRAST("Contrast", false, false, false, INT16U), CONVERTER("Converter", false, true, false, STRING), CREATEDATE("CreateDate", false, false, false, STRING), CUSTOMRENDERED("CustomRendered", false, false, false, INT16U), DATETIMEORIGINAL("DateTimeOriginal", false, false, false, STRING), DIGITALZOOMRATIO("DigitalZoomRatio", false, false, false, RAT64U), EXIFIMAGEHEIGHT("ExifImageHeight", false, false, false, INT16U), EXIFIMAGEWIDTH("ExifImageWidth", false, false, false, INT16U), EXIFVERSION("ExifVersion", false, false, false, UNDEF), EXPOSURE("Exposure", false, true, false, STRING), EXPOSURECOMPENSATION("ExposureCompensation", false, false, false, RAT64S), EXPOSUREINDEX("ExposureIndex", false, false, false, RAT64U), EXPOSUREMODE("ExposureMode", false, false, false, INT16U), EXPOSUREPROGRAM("ExposureProgram", false, false, false, INT16U), EXPOSURETIME("ExposureTime", false, false, false, RAT64U), FILESOURCE("FileSource", false, false, false, UNDEF), FLASH("Flash", false, false, false, INT16U), FLASHENERGY("FlashEnergy", false, false, false, RAT64U), FLASHPIXVERSION("FlashpixVersion", false, false, false, UNDEF), FNUMBER("FNumber", false, false, false, RAT64U), FOCALLENGTH("FocalLength", false, false, false, RAT64U), FOCALLENGTHIN35MMFORMAT("FocalLengthIn35mmFormat", false, false, false, INT16U), FOCALPLANERESOLUTIONUNIT("FocalPlaneResolutionUnit", false, false, false, INT16U), FOCALPLANEXRESOLUTION("FocalPlaneXResolution", false, false, false, RAT64U), FOCALPLANEYRESOLUTION("FocalPlaneYResolution", false, false, false, RAT64U), GAINCONTROL("GainControl", false, false, false, INT16U), GAMMA("Gamma", false, false, false, RAT64U), IMAGEHISTORY("ImageHistory", false, false, false, STRING), IMAGENUMBER("ImageNumber", false, false, false, INT32U), IMAGEUNIQUEID("ImageUniqueID", false, false, false, STRING), ISO("ISO", false, false, false, INT16U), ISOSPEED("ISOSpeed", false, false, false, INT32U), ISOSPEEDLATITUDEYYY("ISOSpeedLatitudeyyy", false, false, false, INT32U), ISOSPEEDLATITUDEZZZ("ISOSpeedLatitudezzz", false, false, false, INT32U), LENS("Lens", false, true, false, STRING), LENSINFO("LensInfo", false, false, false, RAT64U), LENSMAKE("LensMake", false, false, false, STRING), LENSMODEL("LensModel", false, false, false, STRING), LENSSERIALNUMBER("LensSerialNumber", false, false, false, STRING), LIGHTSOURCE("LightSource", false, false, false, INT16U), MAXAPERTUREVALUE("MaxApertureValue", false, false, false, RAT64U), METERINGMODE("MeteringMode", false, false, false, INT16U), MOIREFILTER("MoireFilter", false, true, false, STRING), OFFSETSCHEMA("OffsetSchema", false, false, false, INT32S), OWNERNAME("OwnerName", false, false, false, STRING), PADDING("Padding", false, false, false, UNDEF), RAWFILE("RawFile", false, true, false, STRING), RECOMMENDEDEXPOSUREINDEX("RecommendedExposureIndex", false, false, false, INT32U), RELATEDSOUNDFILE("RelatedSoundFile", false, false, false, STRING), SATURATION("Saturation", false, false, false, INT16U), SCENECAPTURETYPE("SceneCaptureType", false, false, false, INT16U), SCENETYPE("SceneType", false, false, false, UNDEF), SECURITYCLASSIFICATION("SecurityClassification", false, false, false, STRING), SELFTIMERMODE("SelfTimerMode", false, false, false, INT16U), SENSINGMETHOD("SensingMethod", false, false, false, INT16U), SENSITIVITYTYPE("SensitivityType", false, false, false, INT16U), SERIALNUMBER("SerialNumber", false, false, false, STRING), SHADOWS("Shadows", false, true, false, STRING), SHARPNESS("Sharpness", false, false, false, INT16U), SHUTTERSPEEDVALUE("ShutterSpeedValue", false, false, false, RAT64S), SMOOTHNESS("Smoothness", false, true, false, STRING), SPECTRALSENSITIVITY("SpectralSensitivity", false, false, false, STRING), STANDARDOUTPUTSENSITIVITY("StandardOutputSensitivity", false, false, false, INT32U), SUBJECTAREA("SubjectArea", false, false, false, INT16U), SUBJECTDISTANCE("SubjectDistance", false, false, false, RAT64U), SUBJECTDISTANCERANGE("SubjectDistanceRange", false, false, false, INT16U), SUBJECTLOCATION("SubjectLocation", false, false, false, INT16U), SUBSECTIME("SubSecTime", false, false, false, STRING), SUBSECTIMEDIGITIZED("SubSecTimeDigitized", false, false, false, STRING), SUBSECTIMEORIGINAL("SubSecTimeOriginal", false, false, false, STRING), TIMEZONEOFFSET("TimeZoneOffset", false, false, false, INT16S), USERCOMMENT("UserComment", false, false, false, UNDEF), WHITEBALANCE("WhiteBalance", false, false, false, INT16U);

	private final boolean avoided;
	private final boolean unsafe;
	private final boolean protectedField;
	private final String name;
	private final DataType type;

	/**
	 * @param avoided indicate that the tag is avoided
	 * @param unsafe indicated that the tag is unsafe
	 * @param protectedField indicate that the tag is protected
	 * @param name the name of the tag
	 * @param type the data type of the tag
	 */
	private ExifIFD(final String name, final boolean unsafe, final boolean avoided, final boolean protectedField, final DataType type) {
		this.avoided = avoided;
		this.unsafe = unsafe;
		this.protectedField = protectedField;
		this.name = name;
		this.type = type;
	}

}
