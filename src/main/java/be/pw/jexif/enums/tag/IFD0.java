/*******************************************************************************
 * Copyright 2012 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.enums.tag;

import static be.pw.jexif.enums.DataType.FLOAT;
import static be.pw.jexif.enums.DataType.INT16U;
import static be.pw.jexif.enums.DataType.INT32U;
import static be.pw.jexif.enums.DataType.INT8U;
import static be.pw.jexif.enums.DataType.RAT64S;
import static be.pw.jexif.enums.DataType.RAT64U;
import static be.pw.jexif.enums.DataType.STRING;
import static be.pw.jexif.enums.DataType.UNDEF;
import be.pw.jexif.enums.DataType;
import lombok.Getter;

/**
 * Implementation of the IFD0 group: <a href="http://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/EXIF.html">http://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/EXIF.html</a>.
 *
 * @author phillip
 */
@Getter
public enum IFD0 implements Tag {

	ANALOGBALANCE("AnalogBalance", true, false, false, RAT64U), ARTIST("Artist", false, false, false, STRING), ASSHOTICCPROFILE("AsShotICCProfile", true, false, false, UNDEF), ASSHOTNEUTRAL("AsShotNeutral", true, false, false, RAT64U), ASSHOTPREPROFILEMATRIX("AsShotPreProfileMatrix", true, false, false, RAT64S), ASSHOTPROFILENAME("AsShotProfileName", true, false, false, STRING), ASSHOTWHITEXY("AsShotWhiteXY", true, false, false, RAT64U), BASELINEEXPOSURE("BaselineExposure", true, false, false, RAT64S), BASELINENOISE("BaselineNoise", true, false, false, RAT64U), BASELINESHARPNESS("BaselineSharpness", true, false, false, RAT64U), BITSPERSAMPLE("BitsPerSample", true, false, false, INT16U), CALIBRATIONILLUMINANT1("CalibrationIlluminant1", true, false, false, INT16U), CALIBRATIONILLUMINANT2("CalibrationIlluminant2", true, false, false, INT16U), CAMERACALIBRATION1("CameraCalibration1", true, false, false, RAT64S), CAMERACALIBRATION2("CameraCalibration2", true, false, false, RAT64S), CAMERACALIBRATIONSIG("CameraCalibrationSig", true, false, false, STRING), CAMERASERIALNUMBER("CameraSerialNumber", false, false, false, STRING), CELLLENGTH("CellLength", true, false, false, INT16U), CELLWIDTH("CellWidth", true, false, false, INT16U), COLORIMETRICREFERENCE("ColorimetricReference", true, false, false, INT16U), COLORMATRIX1("ColorMatrix1", true, false, false, RAT64S), COLORMATRIX2("ColorMatrix2", true, false, false, RAT64S), COMPRESSION("Compression", true, false, false, INT16U), COPYRIGHT("Copyright", false, false, false, STRING), CURRENTICCPROFILE("CurrentICCProfile", true, false, false, UNDEF), CURRENTPREPROFILEMATRIX("CurrentPreProfileMatrix", true, false, false, RAT64S), DNGBACKWARDVERSION("DNGBackwardVersion", true, false, false, INT8U), DNGLENSINFO("DNGLensInfo", false, false, false, RAT64U), DNGVERSION("DNGVersion", true, false, false, INT8U), DOCUMENTNAME("DocumentName", false, false, false, STRING), DOTRANGE("DotRange", false, false, false, STRING), FILLORDER("FillOrder", true, false, false, INT16U), FORWARDMATRIX1("ForwardMatrix1", true, false, false, RAT64S), FORWARDMATRIX2("ForwardMatrix2", true, false, false, RAT64S), GRAYRESPONSEUNIT("GrayResponseUnit", false, false, false, INT16U), HALFTONEHINTS("HalftoneHints", false, false, false, INT16U), HOSTCOMPUTER("HostComputer", false, false, false, STRING), IMAGEDESCRIPTION("ImageDescription", false, false, false, STRING), IMAGEHEIGHT("ImageHeight", true, false, false, INT32U), IMAGESOURCEDATA("ImageSourceData", true, false, false, UNDEF), IMAGEWIDTH("ImageWidth", true, false, false, INT32U), INKSET("InkSet", false, false, false, INT16U), IPTCNAA("IPTC-NAA", true, false, false, INT32U), LINEARRESPONSELIMIT("LinearResponseLimit", true, false, false, RAT64U), LOCALIZEDCAMERAMODEL("LocalizedCameraModel", false, false, false, STRING), MAKE("Make", false, false, false, STRING), MAKERNOTESAFETY("MakerNoteSafety", false, false, false, INT16U), MAXSAMPLEVALUE("MaxSampleValue", false, false, false, INT16U), MINSAMPLEVALUE("MinSampleValue", false, false, false, INT16U), MODEL("Model", false, false, false, STRING), MODIFYDATE("ModifyDate", false, false, false, STRING), OLDSUBFILETYPE("OldSubfileType", true, false, false, INT16U), ORIENTATION("Orientation", false, false, false, INT16U), ORIGINALRAWFILEDATA("OriginalRawFileData", true, false, false, UNDEF), ORIGINALRAWFILEDIGEST("OriginalRawFileDigest", true, false, false, INT8U), ORIGINALRAWFILENAME("OriginalRawFileName", true, false, false, STRING), PAGENAME("PageName", false, false, false, STRING), PAGENUMBER("PageNumber", false, false, false, INT16U), PANASONICTITLE("PanasonicTitle", false, false, false, UNDEF), PANASONICTITLE2("PanasonicTitle2", false, false, false, UNDEF), PHOTOMETRICINTERPRETATION("PhotometricInterpretation", true, false, false, INT16U), PLANARCONFIGURATION("PlanarConfiguration", true, false, false, INT16U), PREDICTOR("Predictor", true, false, false, INT16U), PREVIEWAPPLICATIONNAME("PreviewApplicationName", true, false, false, STRING), PREVIEWAPPLICATIONVERSION("PreviewApplicationVersion", true, false, false, STRING), PREVIEWCOLORSPACE("PreviewColorSpace", true, false, false, INT32U), PREVIEWDATETIME("PreviewDateTime", true, false, false, STRING), PREVIEWSETTINGSDIGEST("PreviewSettingsDigest", true, false, false, INT8U), PREVIEWSETTINGSNAME("PreviewSettingsName", true, false, false, STRING), PRIMARYCHROMATICITIES("PrimaryChromaticities", false, false, false, RAT64U), PRINTIM("PrintIM", false, false, false, UNDEF), PROCESSINGSOFTWARE("ProcessingSoftware", false, false, false, STRING), PROFILECALIBRATIONSIG("ProfileCalibrationSig", true, false, false, STRING), PROFILECOPYRIGHT("ProfileCopyright", true, false, false, STRING), PROFILEEMBEDPOLICY("ProfileEmbedPolicy", true, false, false, INT32U), PROFILEHUESATMAPDATA1("ProfileHueSatMapData1", true, false, false, FLOAT), PROFILEHUESATMAPDATA2("ProfileHueSatMapData2", true, false, false, FLOAT), PROFILEHUESATMAPDIMS("ProfileHueSatMapDims", true, false, false, INT32U), PROFILELOOKTABLEDATA("ProfileLookTableData", true, false, false, FLOAT), PROFILELOOKTABLEDIMS("ProfileLookTableDims", true, false, false, INT32U), PROFILENAME("ProfileName", true, false, false, STRING), PROFILETONECURVE("ProfileToneCurve", true, false, false, FLOAT), RATING("Rating", false, true, false, INT16U), RATINGPERCENT("RatingPercent", false, true, false, INT16U), RAWDATAUNIQUEID("RawDataUniqueID", true, false, false, INT8U), RAWIMAGEDIGEST("RawImageDigest", true, false, false, INT8U), REDUCTIONMATRIX1("ReductionMatrix1", true, false, false, RAT64S), REDUCTIONMATRIX2("ReductionMatrix2", true, false, false, RAT64S), REFERENCEBLACKWHITE("ReferenceBlackWhite", false, false, false, RAT64U), RESOLUTIONUNIT("ResolutionUnit", false, false, false, INT16U), ROWSPERSTRIP("RowsPerStrip", true, false, false, INT32U), SAMPLESPERPIXEL("SamplesPerPixel", true, false, false, INT16U), SEMINFO("SEMInfo", false, false, false, STRING), SHADOWSCALE("ShadowScale", true, false, false, RAT64U), SOFTWARE("Software", false, false, false, STRING), SUBFILETYPE("SubfileType", true, false, false, INT32U), THRESHOLDING("Thresholding", true, false, false, INT16U), TILELENGTH("TileLength", true, false, false, INT32U), TILEWIDTH("TileWidth", true, false, false, INT32U), TRANSFERFUNCTION("TransferFunction", true, false, false, INT16U), UNIQUECAMERAMODEL("UniqueCameraModel", false, false, false, STRING), WHITEPOINT("WhitePoint", false, false, false, RAT64U), XPAUTHOR("XPAuthor", false, false, false, INT8U), XPCOMMENT("XPComment", false, false, false, INT8U), XPKEYWORDS("XPKeywords", false, false, false, INT8U), XPOSITION("XPosition", false, false, false, RAT64U), XPSUBJECT("XPSubject", false, false, false, INT8U), XPTITLE("XPTitle", false, false, false, INT8U), XRESOLUTION("XResolution", false, false, false, RAT64U), YCBCRCOEFFICIENTS("YCbCrCoefficients", true, false, false, RAT64U), YCBCRPOSITIONING("YCbCrPositioning", true, false, false, INT16U), YCBCRSUBSAMPLING("YCbCrSubSampling", true, false, false, INT16U), YPOSITION("YPosition", false, false, false, RAT64U), YRESOLUTION("YResolution", false, false, false, RAT64U), INTEROPINDEX("InteropIndex", true, false, false, STRING), INTEROPVERSION("InteropVersion", true, false, false, UNDEF), RELATEDIMAGEFILEFORMAT("RelatedImageFileFormat", true, false, false, STRING), RELATEDIMAGEHEIGHT("RelatedImageHeight", true, false, false, INT16U), RELATEDIMAGEWIDTH("RelatedImageWidth", true, false, false, INT16U), ACTIVEAREA("ActiveArea", true, false, false, INT32U), ANTIALIASSTRENGTH("AntiAliasStrength", true, false, false, RAT64U), BAYERGREENSPLIT("BayerGreenSplit", true, false, false, INT32U), BESTQUALITYSCALE("BestQualityScale", true, false, false, RAT64U), BLACKLEVEL("BlackLevel", true, false, false, RAT64U), BLACKLEVELREPEATDIM("BlackLevelRepeatDim", true, false, false, INT16U), CHROMABLURRADIUS("ChromaBlurRadius", true, false, false, RAT64U), DEFAULTCROPORIGIN("DefaultCropOrigin", true, false, false, INT32U), DEFAULTCROPSIZE("DefaultCropSize", true, false, false, INT32U), DEFAULTSCALE("DefaultScale", true, false, false, RAT64U), LINEARIZATIONTABLE("LinearizationTable", true, false, false, INT16U), MASKEDAREAS("MaskedAreas", true, false, false, INT32U), NOISEREDUCTIONAPPLIED("NoiseReductionApplied", true, false, false, RAT64U), WHITELEVEL("WhiteLevel", true, false, false, INT32U);

	private final boolean avoided;
	private final boolean unsafe;
	private final boolean protectedField;
	private final String name;
	private final DataType type;

	/**
	 * @param avoided indicate that the tag is avoided
	 * @param unsafe indicated that the tag is unsafe
	 * @param protectedField indicate that the tag is protected
	 * @param name the name of the tag
	 * @param type the data type of the tag
	 */
	private IFD0(final String name, final boolean unsafe, final boolean avoided, final boolean protectedField, final DataType type) {
		this.avoided = avoided;
		this.unsafe = unsafe;
		this.protectedField = protectedField;
		this.name = name;
		this.type = type;
	}

}
