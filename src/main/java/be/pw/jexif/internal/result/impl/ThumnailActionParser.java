/*******************************************************************************
 * Copyright 2014 P_W999
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.jexif.internal.result.impl;

import java.util.List;

import com.google.common.base.Preconditions;

import be.pw.jexif.enums.Errors;
import be.pw.jexif.internal.action.IAction;
import be.pw.jexif.internal.action.impl.ThumbnailAction;
import be.pw.jexif.internal.constants.ExecutionConstant;
import be.pw.jexif.internal.result.IResultParser;
import be.pw.jexif.internal.util.Cal10nUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PACKAGE)	// use the factory to create a new instance
class ThumnailActionParser implements IResultParser {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void parse(final IAction action, final String actionUID, final List<String> result) {
		Preconditions.checkArgument(validFor().isAssignableFrom(action.getClass()), Cal10nUtil.get(Errors.PARSER_MISMATCH, getClass().getSimpleName(), action.getClass().getSimpleName()));
		Preconditions.checkArgument(action.getId().equals(actionUID), "Invalid action UID");

		boolean possibleError = false;
		for (String line : result) {
			if (line.contains("already exists") || line.contains("0 output files created") || line.contains("1 files could not be read") || line.contains("Error: Unknown file type")) {
				possibleError = true;
			}
		}
		if (result.stream().filter(s -> s.equals(ExecutionConstant.EXIFTOOLREADY)).count() > 4) {
			possibleError = true;
		}
		if (possibleError) {
			log.warn("There may be an error creating the thumbnail image: {}", result);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<? extends IAction> validFor() {
		return ThumbnailAction.class;
	}

}
